//
//  UIView+showError.swift
//  ShareCar
//
//  Created by lishuai on 2018/7/7.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func showError(_ message: String) {
        XHProgressHUD.showText(text: message)
        
    }
    
    func showError(_ message: Message) {
        if message.code == 1005 {
            self.alertWithCancelButton(message: "登录信息已过期，是否重新登录？") {
                let storyBoard = UIStoryboard(name: "UserInfo", bundle: nil)
                let vc = storyBoard.instantiateViewController(withIdentifier: "LoginViewController")
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            self.showError(message.msg)
        }
    }
    
    func alert(title: String? = nil, message: String, completion: (() -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "确定", style: .default) { action in
            completion?()
        }
        alert.addAction(action)

        self.present(alert, animated: true, completion: nil)
    }
    
    func alertWithCancelButton(title: String? = nil, message: String, okTitle: String? = "确定", cancelTitle: String? = "取消", okStyle: UIAlertActionStyle = .default, completion: (() -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: okTitle, style: okStyle) { action in
            completion?()
        }
        alert.addAction(action)
        
        
        let cancel = UIAlertAction(title: cancelTitle, style: .cancel)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func actionSheetchooseLinkman(_ handle:@escaping (String) -> ()) {
        
        let items = ["父母","子女","亲戚","好友"]
        let actionSheet = UIAlertController(title: "", message: "请选择", preferredStyle: .actionSheet)
        for item in items {
            actionSheet.addAction(UIAlertAction(title: item, style: .default) { ac in handle(item)})
        }
        actionSheet.addAction(UIAlertAction(title: "取消", style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
}

extension UIViewController {
    var userLocation: CLLocationCoordinate2D? {
        var mainVC: MainViewController?
        
        if let views = self.navigationController?.viewControllers {
            for i in views where i is MainViewController {
                if let main = i as? MainViewController {
                    mainVC = main
                    break
                }
            }
        } else if let pvc = self.parentIQContainerViewController(),
            let views = pvc.navigationController?.viewControllers {
            for i in views where i is MainViewController {
                if let main = i as? MainViewController {
                    mainVC = main
                    break
                }
            }
        }
        
        return mainVC?.mapView.userLocation.location.coordinate
    }
    
    
}
