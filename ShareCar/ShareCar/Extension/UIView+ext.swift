//
//  ext.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/8/11.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
extension UIView {
    
    enum EmptyType: String, Codable {
        case coupon = "coupon"       //暂无优惠券
        case recard = "recard"       //暂无行程记录
    }
    
    static var statusView :EmptyDataStatusInfo!
    static func showInfoViewWithEmpty(_ view: UIView, text: String, type: EmptyType? = .coupon) {        
        let infoView = Bundle.main.loadNibNamed("EmptyDataStatusInfo", owner: self, options: nil)?.last
        guard let tipView = infoView as? EmptyDataStatusInfo else {
            return
        }
        tipView.imageView.image = UIImage(named: "unhaveRecard")
        //全局变量保存这个view
        statusView = tipView
        tipView.tipLabel.text = text
        view.addSubview(statusView)
        
        statusView.snp.makeConstraints({ make in
            make.width.height.equalTo(200)
            make.centerX.equalTo(view)
            make.centerY.equalTo(view).offset(-80)
        })
    }
    
    //移除这个view
    static func removeStatusView() {
        if statusView != nil {
            statusView.removeFromSuperview()
        }        
    }
}



