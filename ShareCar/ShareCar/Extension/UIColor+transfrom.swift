//
//  UIColor+transfrom.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/9.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

extension UIColor{
    public convenience init(valueRGB: UInt, alpha: CGFloat) {
        self.init(
            red: CGFloat((valueRGB & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((valueRGB & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(valueRGB & 0x0000FF) / 255.0,
            alpha : alpha
        )
        
    }
    
}
