//
//  UserDefaults+custom.swift
//  ShareCar
//
//  Created by lishuai on 2018/8/2.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

extension UserDefaults {
    func saveCustomObject(customObject object: NSCoding, key: String) {
        let encodedObject = NSKeyedArchiver.archivedData(withRootObject: object)
        self.set(encodedObject, forKey: key)
    }
    
    func getCustomObject(forKey key: String) -> AnyObject? {
        let decodedObject = self.object(forKey: key) as? NSData
        
        if let decoded = decodedObject {
            let object = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data)
            return object as AnyObject
        }
        
        return nil
    }
}
