//
//  String+ext.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/8/2.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

extension String {
    
    func toNSRange(_ range: Range<String.Index>) -> NSRange {
        guard let from = range.lowerBound.samePosition(in: utf16), let to = range.upperBound.samePosition(in: utf16) else {
            return NSMakeRange(0, 0)
        }
        return NSMakeRange(utf16.distance(from: utf16.startIndex, to: from), utf16.distance(from: from, to: to))
    }
    
    func transformToPinYin() -> String {
        let mutableString = NSMutableString(string: self)
        CFStringTransform(mutableString, nil, kCFStringTransformToLatin, false)
        CFStringTransform(mutableString, nil, kCFStringTransformStripDiacritics, false)
        return String(mutableString)
    }
    
    func removeAllSpace() -> String {
        return self.replacingOccurrences(of: " ", with: "", options: .literal, range: nil)
    }
    
    
    
    func hw_exMatchStrRange(_ matchStr: String) -> [NSRange] {
    var selfStr = self as NSString
    var withStr = Array(repeating: "X", count: (matchStr as NSString).length).joined(separator: "") //辅助字符串
    if matchStr == withStr { withStr = withStr.lowercased() } //临时处理辅助字符串差错
    var allRange = [NSRange]()
    while selfStr.range(of: matchStr).location != NSNotFound {
    let range = selfStr.range(of: matchStr)
    allRange.append(NSRange(location: range.location,length: range.length))
    selfStr = selfStr.replacingCharacters(in: NSMakeRange(range.location, range.length), with: withStr) as NSString
    }
    return allRange
    }
}
