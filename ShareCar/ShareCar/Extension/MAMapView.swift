//
//  MAMapView.swift
//  ShareCar
//
//  Created by lishuai on 2018/10/23.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import Foundation
import MAMapKit

extension MAMapView {
    func config() {
        self.showsUserLocation = true
        self.userTrackingMode = .follow
        self.zoomLevel = 15
        self.isRotateEnabled = false
        self.isRotateCameraEnabled = false
        self.showsCompass = false
        
        var path = Bundle.main.bundlePath
        path.append("/amap_style.data")
        if let jsonData = NSData(contentsOfFile: path) {
            self.setCustomMapStyleWithWebData(jsonData as Data)
            self.customMapStyleEnabled = true
        }
    }
}
