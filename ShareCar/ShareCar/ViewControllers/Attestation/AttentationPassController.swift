//
//  AttentationPassController.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2019/2/21.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class AttentationPassController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backRoot(_ sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func bindCard(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "bindCardSegue", sender: nil)
    }
    
    @IBAction func baseDeposit(_ sender: UITapGestureRecognizer) {
        performSegue(withIdentifier: "baseDepositSegue", sender: nil)
    }

    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "baseDepositSegue":
            if let vc = segue.destination as? DepositInfoViewController {
                vc.page = .timeRent
                vc.isBoundCard = 1
            }
        default:break
        }
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    

}
