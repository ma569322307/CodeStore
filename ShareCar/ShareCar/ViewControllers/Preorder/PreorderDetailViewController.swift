//
//  PreorderDetailViewController.swift
//  ShareCar
//
//  Created by 李帅 on 2019/2/15.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class PreorderDetailViewController: UIViewController, SegueHandlerType {
    
    enum SegueIdentifier: String {
        case cancelSegue
    }
    
    @IBOutlet weak var carTypeNameLabel: UILabel!
    
    @IBOutlet weak var useCarTimeLabel: UILabel!
    
    @IBOutlet weak var useCarAddressLabel: UILabel!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var preAmountLabel: UILabel!
    
    @IBOutlet weak var plateNumberLabel: UILabel!
    
    @IBOutlet weak var parkingAddressLabel: UILabel!
    
    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var label2: UILabel!
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    
    @IBOutlet weak var submitButton: UIButton!
    
    var preorderId: String?
    
    @IBOutlet weak var preorderDetailLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        loadDetail()
        
        preorderDetailLabel.text = slipViewController?.config?.preorderDetail
    }
    
    func loadDetail() {
        guard let id = preorderId else {
            return
        }
        
        JZLoading.showLoadingWith(toView: self.view)
        Preorder.detail(preorderId: id, success: { [weak self] detail in
            JZLoading.hideLoading()
            
            if let d = detail {
                self?.configureView(detail: d)
            }
        }) { [weak self] msg in
            JZLoading.hideLoading()
            self?.showError(msg)
        }
    }
    
    func configureView(detail: Preorder.Detail) {
        carTypeNameLabel.text = detail.carTypeName
        let time = Date(timeIntervalSince1970: TimeInterval(detail.useCarTime))
        useCarTimeLabel.text = Date.dateConvertString(date: time)
        useCarAddressLabel.text = detail.useCarAddress
        
        statusLabel.text = Preorder.status[detail.status]
        preAmountLabel.text = detail.preAmount
        
        plateNumberLabel.text = detail.plateNumber
        parkingAddressLabel.text = detail.parkingAddress
        
        switch detail.status {
        case 2:
            label1.isHidden = true
            label2.isHidden = true
            plateNumberLabel.isHidden = true
            parkingAddressLabel.isHidden = true
        case 3, 4:
            label2.isHidden = true
            parkingAddressLabel.isHidden = true
        case 5:
            if detail.isCancel == 1 {
                submitButton.isHidden = false
            } else {
                navigationItem.rightBarButtonItem = nil
            }
            
        case 6:
            label2.text = "退款预约费用"
            parkingAddressLabel.text = detail.preRefundAmount
            navigationItem.rightBarButtonItem = nil
        case 7:
            label2.text = "已退款预约费用"
            parkingAddressLabel.text = detail.preRefundAmount
            navigationItem.rightBarButtonItem = nil
        case 8:
            label2.isHidden = true
            parkingAddressLabel.isHidden = true
            navigationItem.rightBarButtonItem = nil
        default:
            break
        }
    }

    @IBAction func cancel(_ sender: UIBarButtonItem) {
        performSegue(.cancelSegue, sender: nil)
    }
    
    @IBAction func rent(_ sender: UIButton) {
        
        guard let app = UIApplication.shared.delegate as? AppDelegate,
            let loc = app.userLocation else {
            return
        }
        
        guard let id = preorderId else {
            return
        }
        
        JZLoading.showLoadingWith(toView: self.view)
        Car.detail(location: loc, preorderId: id, success: { [weak self] data in
            JZLoading.hideLoading()
            if let car = data {
                self?.gotoConfirmPreorder(detail: car)
            }
        }) { [weak self] msg in
            JZLoading.hideLoading()
            self?.showError(msg)
        }
    }
    
    func gotoConfirmPreorder(detail: Car.Detail) {
        weak var nav = navigationController
        nav?.popToRootViewController(animated: false)
        if let main = nav?.viewControllers.first as? MainViewController {
            main.performSegue(.commitOrderSegue, sender: detail)
        }
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(forSegue: segue) {
        case .cancelSegue:
            if let vc = segue.destination as? PreorderCancelViewController {
                vc.preorderId = preorderId
            }
        }
    }

}
