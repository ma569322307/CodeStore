//
//  PreorderCancelViewController.swift
//  ShareCar
//
//  Created by 李帅 on 2019/2/18.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class PreorderCancelViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var otherField: UITextField!
    
    var data = [Preorder.CancelReason.Item]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    var selectedReason: Preorder.CancelReason.Item? {
        didSet {
            otherField.isHidden = selectedReason?.preorderCancelReasonId != "-1"
        }
    }
    
    var preorderId: String?
    
    @IBOutlet weak var preorderCancelLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadCancelReason()
        
        preorderCancelLabel.text = slipViewController?.config?.preorderCancel
    }
    
    func loadCancelReason() {
        JZLoading.showLoadingWith(toView: self.view)
        Preorder.cancelReason(success: { [weak self] data in
            JZLoading.hideLoading()
            if let d = data {
                self?.data = d
                self?.data.append(Preorder.CancelReason.Item(preorderCancelReasonId: "-1", reasonName: "其它"))
            }
        }) { [weak self] msg in
            JZLoading.hideLoading()
            self?.showError(msg)
        }
    }
    
    @IBAction func affirm(_ sender: UIButton) {
        guard let pid = preorderId else {
            return
        }
        
        guard var reason = selectedReason?.reasonName else {
            showError("请您选择取消原因(1030)")
            return
        }
        
        if let id = selectedReason?.preorderCancelReasonId, id == "-1" {
            guard let text = otherField.text, text.count > 0 else {
                showError("请您输入取消原因(1031)")
                return
            }
            
            reason = text
        }
        
        Preorder.cancel(preorderId: pid, reason: reason, success: { [weak self] in
            self?.navigationController?.popToRootViewController(animated: true)
        }) { [weak self] msg in
            self?.showError(msg)
        }
    }

}

extension PreorderCancelViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedReason = data[indexPath.row]
        collectionView.reloadData()
    }
}

extension PreorderCancelViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        
        let item = data[indexPath.row]
        
        if let cell = cell as? ReasonCollectionViewCell {
            cell.layer.borderColor = UIColor(red: 221.0/255.0, green: 221.0/255.0, blue: 221.0/255.0, alpha: 1.0).cgColor
            cell.textLabel.textColor = UIColor.black
            cell.layer.backgroundColor = UIColor.white.cgColor
            
            cell.textLabel.text = item.reasonName
            
            if item.preorderCancelReasonId == selectedReason?.preorderCancelReasonId {
                cell.layer.borderColor = UIColor(red: 252.0/255.0, green: 166.0/255.0, blue: 72.0/255.0, alpha: 1.0).cgColor
                cell.textLabel.textColor = UIColor.white
                cell.layer.backgroundColor = UIColor(valueRGB: 0xFCA648, alpha: 1.0).cgColor
            }
        }
        
        return cell
    }
    
}
