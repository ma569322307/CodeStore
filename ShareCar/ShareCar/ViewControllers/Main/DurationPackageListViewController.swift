//
//  DurationPackageListViewController.swift
//  ShareCar
//
//  Created by lishuai on 2018/10/15.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class DurationPackageListViewController: UITableViewController {

    @IBOutlet weak var infoLabel: UILabel!
    
    var data = [Order.DurationPackageList.Item]() {
        didSet {
            tableView.reloadData()
            
            for (i, v) in data.enumerated() {
                if v.durationPackageId == selectedDurationPackageId {
                    tableView.selectRow(at: IndexPath(row: i, section: 0), animated: false, scrollPosition: UITableViewScrollPosition.none)
                    break
                }
            }
        }
    }
    
    var orderId: String? = nil
    
    var projectTimerentId: String? = nil
    
    var sourceFlag: Int? = nil
    
    var selectedPackageBlock: ((String?, String?) -> ())? = nil
    
    var selectedDurationPackageId: String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        infoLabel.text = slipViewController?.config?.durationPackagePageInfo
        
        loadData()
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        
        if sourceFlag == 1 {
            selectedPackageBlock?(nil, "选择")
            navigationController?.popViewController(animated: true)
            return
        }
        guard let orderId = orderId else {
            showError("订单id不存在")
            return
        }

        JZLoading.showLoadingWith(toView: view)
        Order.saveDurationPackage(orderId: orderId, durationPackageId: nil, success: { [weak self] in
            JZLoading.hideLoading()
            self?.navigationController?.popViewController(animated: true)
        }) { [weak self] message in
            JZLoading.hideLoading()
            self?.showError(message)
        }
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let item = data[indexPath.row]
        
        if let c = cell as? DurationPackageCell {
            c.configure(item: item)
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = data[indexPath.row]
        
        if let cell = tableView.cellForRow(at: indexPath) {
            if sourceFlag == 1 {
                selectedDurationPackageId = item.durationPackageId
                cell.setSelected(true, animated: false)
                selectedPackageBlock?(selectedDurationPackageId, item.packageName)
                navigationController?.popViewController(animated: true)
            } else {
                guard let id = orderId else {
                    showError("订单号错误")
                    return
                }
                
                if item.durationPackageId != selectedDurationPackageId {
                    JZLoading.showLoadingWith(toView: self.view)
                    Order.saveDurationPackage(orderId: id, durationPackageId: item.durationPackageId, success: { [weak self] in
                        JZLoading.hideLoading()
                        self?.selectedPackageBlock?(item.durationPackageId, item.packageName)
                        self?.navigationController?.popViewController(animated: true)
                    }) { [weak self] message in
                        JZLoading.hideLoading()
                        self?.showError(message)
                    }
                }
            }
        }
    }

    func loadData() {
        guard let id = projectTimerentId else {
            return
        }
        
        guard let flag = sourceFlag else {
            return
        }
        
        JZLoading.showLoadingWith(toView: self.view)
        Order.durationPackageList(projectTimerentId: id, sourceFlag: flag, success: { [weak self] items in
            JZLoading.hideLoading()
            if let i = items {
                self?.data = i
            }
        }) { [weak self] message in
            JZLoading.hideLoading()
            self?.showError(message)
        }
    }
}
