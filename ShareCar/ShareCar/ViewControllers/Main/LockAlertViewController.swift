//
//  LockAlertViewController.swift
//  ShareCar
//
//  Created by 李帅 on 2019/1/18.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class LockAlertViewController: UIViewController {

    static let kNoShowLockAlert = "no_show_lock_alert"
    
    var completion: (()->())?
    
    @IBOutlet weak var selectButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func close(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
    @IBAction func neverShow(_ sender: UIButton) {
        selectButton.isSelected.toggle()
    }
    
    @IBAction func lock(_ sender: UIButton) {
        if selectButton.isSelected {
            UserDefaults.standard.set(true, forKey: LockAlertViewController.kNoShowLockAlert)
        }
        completion?()
        dismiss(animated: true)
    }
}


