//
//  ProtocolViewController.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2018/12/4.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class ProtocolViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    
    var commit: ((ProtocolViewController) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = URL(string: kServerName + "../app/getconfig/rent_car_agreement") {
            webView.loadRequest(URLRequest(url: url))
        }
    }

    @IBAction func cancle(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func commitOrder(_ sender: UIButton) {
        commit?(self)
    }
    
    
}
