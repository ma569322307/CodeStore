//
//  CancelOrderPayViewController.swift
//  ShareCar
//
//  Created by lishuai on 2018/7/9.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit

class CancelOrderPayViewController: UIViewController, SegueHandlerType {
    
    enum SegueIdentifier: String {
        case webViewSegue
    }

    @IBOutlet weak var payFeeLabel: UILabel!
    @IBOutlet weak var payFeeLabel2: UILabel!
    @IBOutlet weak var appointDurationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet var paySwith: PaySwith!

    var orderId: String?
    var payFee: String?
    var appointDuration: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        paySwith.addObserve { payment, success in
            switch payment {
            case .aliPay:
                if success {
                    self.navigationController?.popToRootViewController(animated: true)
                } else {
                    XHProgressHUD.showError("你取消了支付")
                }
            case .wePay:
                if success {
                    self.navigationController?.popToRootViewController(animated: true)
                } else {
                    XHProgressHUD.showError("你已取消支付")
                }
            case .balance, .ticket:
                break
            }
        }
        
        if let orderId = orderId {
            JZLoading.showLoadingWith(toView: self.view)
            Order.cancelAmount(orderId: orderId, success: { [weak self] data in
                if let data = data, let self = self {
                    self.payFeeLabel.text = "\(data.payableMoney)元"
                    self.payFeeLabel2.text = "\(data.payableMoney)元"
                    self.appointDurationLabel.text = "\(data.appointDuration)"
                    
                    if data.balanceFlag == 1 {  //需要余额支付
                        
                    } else {    //不需要支付
                        self.paySwith.balance = false
                        self.tableView.reloadData()
                    }
                    JZLoading.hideLoading()
                }
            }) { [weak self] message in
                JZLoading.hideLoading()
                self?.showError(message)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func pay(_ sender: UIButton) {
        guard let orderId = orderId else {
            showError("订单号错误")
            return
        }
        
        guard let p = paySwith.selected else {
            showError("请选择支付方式")
            return
        }
        
        JZLoading.showLoadingWith(toView: self.view)
        Order.cancelOrderPay(orderId: orderId, payment: p, success: { [weak self] data in
            JZLoading.hideLoading()
            
            if let payStr = data?.payStr, let self = self {
                switch p {
                case .aliPay:
                    Pay.zhifuPay(payStr: payStr)
                case .balance:
                    XHProgressHUD.showSuccess("支付成功")
                    self.navigationController?.popToRootViewController(animated: true)
                case .wePay:
                    Pay.wechatPay(payStr: payStr)
                case .ticket:
                    break
                }
            }
        }) { [weak self] error in
            if error.code == 2011 {
                self?.navigationController?.popToRootViewController(animated: true)
            } else {
                self?.showError(error)
            }
        }
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(forSegue: segue) {
        case .webViewSegue:
            if let nav = segue.destination as? UINavigationController,
                let vc = nav.viewControllers.first as? WebViewController {
                vc.page = Page.server(.overtime)
            }
        }
    }
}
