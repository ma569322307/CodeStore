//
//  AboutViewController.swift
//  ShareCar
//
//  Created by lishuai on 2018/8/6.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        versionLabel.text = "密橙出行 V" + UIDevice.current.appVersion()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
