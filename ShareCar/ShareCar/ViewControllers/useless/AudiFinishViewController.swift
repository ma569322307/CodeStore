//
//  AudiFinishViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/8/20.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

class AudiFinishViewController: UIViewController {
    
    @IBOutlet weak var  nameLabel: UILabel!
    @IBOutlet weak var  identityCardLabel: UILabel!
    
    var userName: String?
    var identityCard: String?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let name = userName, let identityCard = identityCard else {
            return
        }
        
        nameLabel.text = name
        identityCardLabel.text = identityCard
    }
}
