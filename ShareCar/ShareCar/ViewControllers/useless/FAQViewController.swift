//
//  FAQViewController.swift
//  ShareCar
//
//  Created by lishuai on 2018/10/30.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class FAQViewController: UITableViewController {

    var data = [Guide.Item]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Guide.getGuide(guideType: 1, page: 1, pageSize: 100, success: { [weak self] data in
            if let d = data?.list {
                self?.data = d
            }
        }) { [weak self] message in
            self?.showError(message)
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        let item = data[indexPath.row]
        cell.textLabel?.text = item.title

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = data[indexPath.row]
        performSegue(withIdentifier: "webSegue", sender: item.url)
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "webSegue" {
            guard let nav = segue.destination as? UINavigationController else {
                return
            }
            
            if let vc = nav.viewControllers.first as? WebViewController {
                if let url = sender as? String {
                    vc.page = Page.URL(url)
                }
            }
        }
    }
}
