//
//  CoerceEndDayOrderController.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2018/12/7.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class CoerceEndDayOrderController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var slider: OilSlider!
    
    var orderId: String!
    var selectIndex: Int!
    var indexPath1: String!
    var indexPath2: String!
    var images = [String]()
    
    var oilValue = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func commit(_ sender: Any) {
        
        guard let desc = textView.text, desc.count > 0 else {
            showError("请描述目前的情况")
            return
        }
        JZLoading.showLoadingWith(toView: view)
        Order.coerceEndOrder(orderId: orderId, descInfo: desc, imagePath: images, lat: userLocation?.latitude, lon: userLocation?.longitude, boardImage: indexPath2, oilStatus: oilValue, success: {
            JZLoading.hideLoading()
            self.showError("强制成功!")
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0, execute: {
                self.performSegue(withIdentifier: "DayReturnDetailViewController", sender: self.orderId)
            })
            
        }) { (err) in
            JZLoading.hideLoading()
            self.showError(err.msg)
        }
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DayReturnDetailViewController" {
            if let vc = segue.destination as? DayReturnDetailViewController {
                vc.orderId = orderId
            }
        }
    }
 
    @IBAction func oilValueChange(_ sender: OilSlider) {
        oilValue = sender.value
    }
}

extension CoerceEndDayOrderController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell:UITableViewCell
//        switch indexPath.row {
//        case 2:
//            cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
//        default:
//            cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//            if let cell = cell as? CoerceDayCell {
//                cell.configCell {
//                    self.selectIndex = indexPath.row
//                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
//                        let  cameraPicker = UIImagePickerController()
//                        cameraPicker.delegate = self
//                        cameraPicker.allowsEditing = true
//                        cameraPicker.sourceType = .camera
//                        //在需要的地方present出来
//                        self.present(cameraPicker, animated: true, completion: nil)
//                    } else {
//                        self.showError("不支持拍照")
//                    }
//                }
//            }
//        }
//        return cell
//    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:UITableViewCell
        switch indexPath.row {
        case 0, 1:
            cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            if let cell = cell as? CoerceDayCell {
                cell.titleLabel.text = indexPath.row == 0 ? "请您拍摄当前油表照片(选填)" : "请提供车辆问题的截图或照片(选填)"
                cell.configCell {
                    self.selectIndex = indexPath.row
                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
                        let  cameraPicker = UIImagePickerController()
                        cameraPicker.delegate = self
                        cameraPicker.allowsEditing = true
                        cameraPicker.sourceType = .camera
                        //在需要的地方present出来
                        self.present(cameraPicker, animated: true, completion: nil)
                    } else {
                        self.showError("不支持拍照")
                    }
                }
            }
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
        }
        return cell
    }

}

extension CoerceEndDayOrderController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let image:UIImage = info[UIImagePickerControllerEditedImage] as! UIImage
        
        //上传图片
        User.uploadFile(image) { [weak self] path in
            if let self = self {
                switch self.selectIndex {
                case 0:
                    self.images.append(path)
                    self.indexPath1 = path
                case 1:self.indexPath2 = path
                default:break
                }
                let cell = self.tableView.cellForRow(at: IndexPath(row: self.selectIndex, section: 0)) as! CoerceDayCell
                cell.btn.setBackgroundImage(image, for: .normal)
            }
        }
        dismiss(animated: true, completion: nil)
    }
}


extension CoerceEndDayOrderController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        tipLabel.isHidden = textView.text.count > 0 ? true : false
    }
    
}


