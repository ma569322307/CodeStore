//
//  GuideViewController.swift
//  ShareCar
//
//  Created by lishuai on 2018/10/12.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class GuideViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view delegate

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 3 {
            performSegue(withIdentifier: "FAQSegue", sender: nil)
        } else if indexPath.row == 4 {
            if let url = kCallCenter {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        } else {
            var page: H5page
            switch indexPath.row {
            case 0: page = H5page.registere
            case 1: page = H5page.privacy
            case 2: page = H5page.refuel
            default: page = H5page.registere
            }
            
            performSegue(withIdentifier: "webSegue", sender: page)
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "webSegue" {
            guard let nav = segue.destination as? UINavigationController else {
                return
            }
            
            guard let vc = nav.viewControllers.first as? WebViewController else {
                return
            }
            
            if let p = sender as? H5page {
                vc.page = Page.server(p)
            }
        }
    }
}
