//
//  File.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/8/24.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class FaceGuideViewController: UIViewController {
    
    var block: (() -> ())?
    
    var orientation: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func chooseButtonClicked(_ sender: UIButton) {
        sender.isSelected.toggle()
        let open = sender.isSelected
        UserDefaults.standard.set(open, forKey: "showGuidePage")
    }
    
    @IBAction func goBack(_ sender: Any) {
        if orientation == 1 {
            navigationController?.popViewController(animated: false)
            block?()
        } else {
            performSegue(withIdentifier: "gotoAuthSence", sender: self)
        }
    }
}
