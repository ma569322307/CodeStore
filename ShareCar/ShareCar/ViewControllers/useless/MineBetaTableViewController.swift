//
//  MineBetaTableViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/10/30.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit
import Foundation

class MineBetaTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var data : User.UnpayRecard?
    var index  = 1
    var page: Int = 1            //默认请求第一页
    
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var leftLine : UIView!
    @IBOutlet weak var InfoView: UIView!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var rightLine : UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func chooseButton(_ sender: UIButton) {
        sender.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)
        
        if sender.tag == 50 {
            leftLine.isHidden = false
            rightLine.isHidden = true
            rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
            index = 1
        } else {
            leftLine.isHidden = true
            rightLine.isHidden = false
            leftButton.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
            index = 2
        }
        //数据请求
        loadData(false, index: index)
    }
    
    func loadData(_ isMore: Bool,index : Int) {
        //如果切换了按钮，则先清除
        if index != index {
            data = nil
            page = 1
        }
        
        JZLoading.showLoadingWith(toView: self.view)
        page = 1
        User.fetchUserDebtInfo(page: page, page_size: 20, payStatus: index, success: { [weak self] data in
            JZLoading.hideLoading()
            if let self = self {
                self.tableView.endRefreshing(isSuccess: true)
                
                self.data = data
                UIView.removeStatusView()
                self.tableView.reloadData()
                if self.data?.list.count == 0{
                    UIView.showInfoViewWithEmpty(self.view, text: "暂无欠款记录")
                    return
                }
            }
        }) { [weak self] message in
            self?.tableView.endRefreshing(isSuccess: true)
            JZLoading.hideLoading()
            self?.showError(message)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data?.list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let testView2 = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 5))
        testView2.backgroundColor = UIColor.init(valueRGB: 0xDCDCDC, alpha: 1.0)
        return testView2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MineUnPayDebtCell
        let model = data?.list[indexPath.section]
        if let model = model {
            cell.config(item: model)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = data?.list[indexPath.section] {
            if model.pay_status == 1 {
                performSegue(withIdentifier: "orderCancleNoPay", sender: model)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func viewDidLoad() {
        tableView.register(UINib(nibName: "SlipInfoCell", bundle: nil), forCellReuseIdentifier: "Identifier")
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        
        let header = CustomRefreshHeader()
        tableView.gtm_addRefreshHeaderView(refreshHeader: header) { [weak self] in
            if let self = self {
                self.loadData(true, index: self.index)
            }
        }
        
        let footer = CustomRefreshFooter()
        tableView.gtm_addLoadMoreFooterView(loadMoreFooter: footer) { [weak self] in
            if let self = self {
                self.loadData(false, index: self.index)
            }
        }
        
        tableView.triggerRefreshing()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "orderCancleNoPay" {
            if let controller = segue.destination as? PayDepositViewController,
                let info = sender as? User.UnpayRecard.Info {
                controller.numberInfPay = Double(info.money)!
                controller.orderId = info.receipt_sn
                controller.workType = .payDebtorder
                controller.needAcccountPay = 0
            }
        }
    }
}
