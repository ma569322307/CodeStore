//
//  RuleViewController.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2018/12/7.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit
class RuleViewController: UIViewController {

    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var leftLine : UIView!
    @IBOutlet weak var InfoView: UIView!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var rightLine : UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        rule(leftButton)
    }
    
    @IBAction func rule(_ sender: UIButton) {
        sender.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)
        
        if sender.tag == 50 {
            leftLine.isHidden = false
            rightLine.isHidden = true
            rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
            
            if let url = URL(string: kServerName + "../" + "app/getconfig/regular") {
                webView.loadRequest(URLRequest(url: url))
            }

        } else {
            leftLine.isHidden = true
            rightLine.isHidden = false
            leftButton.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
            if let url = URL(string: kServerName + "../" + "app/getconfig/day_regular") {
                webView.loadRequest(URLRequest(url: url))
            }
        }
    }
}
