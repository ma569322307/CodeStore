//
//  HomePageViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/6/28.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

class HomePageViewController: UITableViewController {
    let userDefaultImage  = "default_user_photo"
    let userAuthedImage   = "user_auth"
    let telephone         = "telephone"
    let chengValue        = "cheng_value"
    let userUnregister    = "user_weiRegister"
    let userRegister      = "user_auth"
    let manualingImage    = "manualing"
    let RejectImage       = "attestationReject"
    let userHeadPhone     = "default_user"
    let titles            = ["普通会员","蜜月","橙年","购买会员"]
    
    @IBOutlet weak var userPhotoImageview: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var isAuth: UIImageView!
    @IBOutlet weak var storeDescLabel: UILabel!    
    @IBOutlet weak var accountTypeLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        refresh()
    }
    
    func refresh() {
        
        //判断用户是否登录
        if User.isLogin() {
            //已经登录了
            isAuth.isHidden = false
            isAuth.image = UIImage(named: userAuthedImage)

            userPhotoImageview.layer.cornerRadius = 30
            
            if let user = User.info() {
                userPhotoImageview.image = UIImage(named: userDefaultImage)
 
                let index3 = user.telephone.index(user.telephone.startIndex, offsetBy: 3)
                let index4 = user.telephone.index(user.telephone.startIndex, offsetBy: 6)
                let value = user.telephone.replacingCharacters(in: index3...index4, with: "****")
                userNameLabel.text = value

                storeDescLabel.text = "橙意值:\(user.chengValue)"
                if user.certFlag == 2 {
                    isAuth.image = UIImage(named: userRegister)
                } else {
                    switch user.certStep {
                    case 0,1:isAuth.image = UIImage(named: userUnregister)
                    case 2:       isAuth.image = UIImage(named: manualingImage)
                    case 3:          isAuth.image = UIImage(named: RejectImage)
                    case 4:        isAuth.image = UIImage(named: userRegister)
                    default:break
                    }
                }

                if user.memberType > 0 && user.memberType < 4 {
                    self.accountTypeLabel.text = titles[user.memberType - 1]
                }

                if user.avatarPath != "" {
                    let offerUrl = URL(string: kImageServerName + user.avatarPath)
                    userPhotoImageview.kf.setImage(with: offerUrl)
                }
            }
        } else {
            isAuth.isHidden = true
            accountTypeLabel.text = "购买会员"
            userPhotoImageview.image = UIImage(named: userHeadPhone)
            userPhotoImageview.layer.cornerRadius = 0
            userNameLabel.text = "未登录"
            storeDescLabel.text = "登录后可享受更多特权"
        }
    }
    
    @IBAction func userLoginStatus(_ sender: UIControl) {
        if User.isLogin() {
            performSegue(withIdentifier: "userInfoScene", sender: self)
        } else {
            performSegue(withIdentifier: "loginScene", sender: self)
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if User.isLogin() {
            switch indexPath.row {
            case 0: performSegue(withIdentifier: "huiyuanSence", sender: self)
            case 1: performSegue(withIdentifier: "WalletSence", sender: self)
            case 2: performSegue(withIdentifier: "mineDebtSence", sender: self)
            case 3: performSegue(withIdentifier: "xingchengSence", sender: self)
            case 4: performSegue(withIdentifier: "weizhangSence", sender: self)
            case 5: performSegue(withIdentifier: "userGuideSence", sender: self)
            case 6: performSegue(withIdentifier: "shezhiSence", sender: self)
            default: return
            }
        } else {
            performSegue(withIdentifier: "loginScene", sender: self)
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}
