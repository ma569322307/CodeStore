//
//  SweetmonViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/4.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class SweetmonViewController: UIViewController {
    
    
    var memberInfo : [String : AnyObject]?
    var monthMember : [String : AnyObject]?
    var yearMember : [String : AnyObject]?
    
    @IBOutlet weak var leftCardControl : UIControl!
    @IBOutlet weak var rightCardControl : UIControl!
    
    @IBOutlet weak var  left001 : UILabel!
    @IBOutlet weak var  left002 : UILabel!
    @IBOutlet weak var  left003 : UILabel!
    @IBOutlet weak var left004: UIView!
    @IBOutlet weak var leftImageView: UIImageView!
    
    @IBOutlet weak var  right001 : UILabel!
    @IBOutlet weak var  right002 : UILabel!
    @IBOutlet weak var  right003 : UILabel!
    @IBOutlet weak var right004: UIView!
    @IBOutlet weak var rightImageView: UIImageView!
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var  cardTyleLabel : UILabel!
    @IBOutlet weak var  cardEndDataLabel : UILabel!
    @IBOutlet weak var  cardDescLabel : UILabel!
    @IBOutlet weak var payedCardInfo: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var tipDepositLabel: UILabel!
    let month = 99.00
    let year = 399.00
    var index = 0
    
    var data: User.MemberInfo?
    
    @IBAction func chooseWay(_ sender :UIControl){
        if sender.tag == 40 {
            index = 0
            left001.textColor = UIColor.white
            left002.textColor = UIColor.white
            left003.textColor = UIColor.white
            left004.backgroundColor = UIColor.white
            leftImageView.image = UIImage(named: "99")
            
            right001.textColor = UIColor.black
            right002.textColor = UIColor.black
            right003.textColor = UIColor.black
            right004.backgroundColor = UIColor.lightGray
            rightImageView.image = UIImage(named: "399")
        } else {
            index = 1
            left001.textColor = UIColor.black
            left002.textColor = UIColor.black
            left003.textColor = UIColor.black
            left004.backgroundColor = UIColor.lightGray
            leftImageView.image = UIImage(named: "399")
            
            right001.textColor = UIColor.white
            right002.textColor = UIColor.white
            right003.textColor = UIColor.white
            right004.backgroundColor = UIColor.white
            rightImageView.image = UIImage(named: "99")
        }
    }
    
    @IBAction func continuePay(_ sender : UIButton) {
        performSegue(withIdentifier: "ViPSence", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViPSence" {
            
            if let controller = segue.destination as? PayDepositViewController {
                controller.numberInfPay = index == 0 ? month : year
                controller.workType = .payCard
                if index == 0{
                    if let needAccoundPay = data?.monthMember.balanceFlag {
                        controller.needAcccountPay = needAccoundPay
                    }
                } else {
                    if let needAccoundPay = data?.yearMember.balanceFlag {
                        controller.needAcccountPay = needAccoundPay
                    }
                }
                controller.cardType = PayDepositViewController.JZCardType(rawValue:index) ?? PayDepositViewController.JZCardType.monthCard
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //获取付费会员
        JZLoading.showLoadingWith(toView: self.view)
        User.getUsermoneyInfo(success: { [weak self] data in
            JZLoading.hideLoading()
            if let self = self {
            self.backgroundView.isHidden = false
            
            self.data = data
            
                if let data = data {
                    if data.memberInfo == nil {
                        self.heightConstraint.constant = 0
                    } else {
                        self.payButton.setTitle("立即续费", for: .normal)
                        self.heightConstraint.constant = 120
                        self.payedCardInfo.isHidden = false
                        if let memberInfo = data.memberInfo {
                            self.cardTyleLabel.text = memberInfo.memberType == 1 ? "蜜月" : "橙年"
                            self.tipDepositLabel.isHidden = memberInfo.memberType == 1 ? true : false
                            self.cardEndDataLabel.text = memberInfo.expiryDate
                            self.cardDescLabel.text = "里程费和时长费部分\(memberInfo.discount)折"
                        }
                    }
                    //填充月卡信息
                    self.left001.text = "\(data.monthMember.expiryDays)天(蜜月)"
                    self.left002.text = "￥\(data.monthMember.realPrice)"
                    self.left003.text = "￥\(data.monthMember.originalPrice)"
                    self.monthLabel.text = data.monthMember.brief
                    
                    self.right001.text = "\(data.yearMember.expiryDays)天(橙年)"
                    self.right002.text = "￥\(data.yearMember.realPrice)"
                    self.right003.text = "￥\(data.yearMember.originalPrice)"
                    self.yearLabel.text = data.yearMember.brief
                }
            }
        }) { [weak self] message in
            JZLoading.hideLoading()
            self?.showError(message)
        }
    }
}
