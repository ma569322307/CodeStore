//
//  OrderDayDetailController.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2018/12/10.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class OrderDayDetailController: UIViewController {
    
    var orderId: String!
    
    @IBOutlet weak var beginTimeLabel: UILabel!
    @IBOutlet weak var usedTimeLabel: UILabel!
    
    @IBOutlet weak var startCommitmoneyLabel: UILabel!
    @IBOutlet weak var dayMoneyLabel: UILabel!
    @IBOutlet weak var otherMoneyLabel: UILabel!
    @IBOutlet weak var unCountLabel: UILabel!
    @IBOutlet weak var orderTotalAccountLabel: UILabel!
    @IBOutlet weak var orderTotalLabel: UILabel!
    @IBOutlet weak var otherTotalLabel: UILabel!
    @IBOutlet weak var unCountTotalLabel: UILabel!
    @IBOutlet weak var fujiaTotalLabel: UILabel!
    @IBOutlet weak var oilAndOtherLabel: UILabel!
    @IBOutlet weak var retuanToAccountLabel: UILabel!
    
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var leftTitleLabel: UILabel!
    @IBOutlet weak var rightTitleLabel: UILabel!
    
    var data: User.OrderDetailDayInfo?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let rightButton = UIBarButtonItem(title: "申诉/投诉", style: .plain, target: self, action: #selector(complain))
        navigationItem.rightBarButtonItem = rightButton

        
        JZLoading.showLoadingWith(toView: view)
        User.getOrderDetailForDayInfo(orderId: orderId, success: { [weak self] data in
            JZLoading.hideLoading()
            self?.data = data
            if let data = data, let self = self {
                self.showOrderDetailView(data)
            }
        }) { error in
            self.remove()
            JZLoading.hideLoading()
            self.showError(error.msg)
        }
    }
    @objc func complain(){
        
            performSegue(withIdentifier: "judgecomplainSence", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "judgecomplainSence" {
            if let controller = segue.destination as? ComplainViewController {
                controller.dayData = data
                controller.orderId = orderId
            }
        }
    }


    
    func showOrderDetailView(_ data: User.OrderDetailDayInfo)  {
        
        beginTimeLabel.text = Date.timeStampToString(timeStamp: data.beginTime)
        usedTimeLabel.text = Date.timeStampToString(timeStamp: data.endTime)
        startCommitmoneyLabel.text = data.paidAmountData.prepayAmount
        dayMoneyLabel.text = data.paidAmountData.rentAmount
        otherMoneyLabel.text = data.paidAmountData.prepareAmount
        unCountLabel.text = data.paidAmountData.noDeductibles
        orderTotalAccountLabel.text = data.totalAmountData.totalAmount
        orderTotalLabel.text = data.totalAmountData.rentAmount
        otherTotalLabel.text = data.totalAmountData.prepareAmount
        unCountTotalLabel.text = data.totalAmountData.noDeductibles
        fujiaTotalLabel.text = data.totalAmountData.returnAmount
        oilAndOtherLabel.text = data.totalAmountData.oilAmount
        retuanToAccountLabel.text = data.payableAmount
        
        if data.isNeedPay == .refund {
            bottomView.isHidden = false
        }
        leftTitleLabel.text = "预定时缴纳订单金额(预定天数:\(data.totalAmountData.realRentDays))"
        rightTitleLabel.text = "实际订单金额(实际天数:\(data.paidAmountData.rentDays)天)"
    }
    
    func remove() {
        for v in view.subviews {
            v.removeAllSubviews()
        }
    }
    /*orderTotalAccountLabe
    // MorderTotalLabel: UILaARK: - Navigation
otherTotalLabel: UILa
    // IunCountTotalLabel: UIn a storyboard-based application, you will often want to do a little preparation before navigation
    overfujiaTotalLabel: UILaride func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        oilAndOtherLabel: UIL// Get the new view controller using segue.destination.
        retuanToAccountLabel:// Pass the selected object to the new view controller.
    }
    */

}
