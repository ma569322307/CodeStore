//
//  SettingViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/5.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class SettingViewController: UIViewController {
    
    var titles = ["关于我们"]
    @IBAction func outlogin(_ sender: UIButton) {
        User.logout()
        slipViewController?.menuViewController?.refresh()
        navigationController?.popToRootViewController(animated: true)
    }
}

extension SettingViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = titles[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: performSegue(withIdentifier: "aboutOursSence", sender: nil)
            case 1:performSegue(withIdentifier: "addAccountSence", sender: nil)
        default:break
        }
    }
}
