//
//  DepositInfoViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/10.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

class DepositInfoViewController: UIViewController {
    
    enum Page: Int {
        case timeRent = 1        //时租
        case dateRent = 2        //日租
        case dateReserve = 3     //日租预定
    }
    
    var isBoundCard: Int!
    
    @IBOutlet weak var topConst: NSLayoutConstraint!
    @IBOutlet weak var topTitleLabel: UILabel!
    @IBOutlet weak var topAmountLabel: UILabel!
    
    @IBOutlet weak var tipImage: UIImageView!
    @IBOutlet weak var botBtn: UIButton!
    @IBOutlet weak var topBtn: UIButton!
    @IBOutlet weak var bottomTitleLabel: UILabel!
    @IBOutlet weak var bottomAmountLabel: UILabel!
    @IBOutlet weak var bottomTipLabel: UILabel!
    var page: Page!
    var data : User.Deposit?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = page == .timeRent ? "基础押金":
                               page == .dateRent ? "日租保证金":
                               page == .dateReserve ? "缴纳金额":""
        bottomTipLabel.text = slipViewController?.config?.depositInstruction
        if let value = slipViewController?.config?.depositInstruction, value.count == 0 {
            tipImage.isHidden = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //网络请求
        quest()
        self.topConst.constant = 0
    }
    
    func quest() {
        JZLoading.showLoadingWith(toView: self.view)
        //获取押金
        User.deposit(status:page.rawValue,success: { [weak self] data in
            JZLoading.hideLoading()
            self?.data = data
            if let data = data, let self = self {
                //更新用户信息
                if let user = User.info() {
                    if self.page == .timeRent {
                        
                        if data.depositStatus == 1 {
                            user.isDeposit = 2
                        } else if data.depositStatus == 2 {
                            user.isDeposit = 1
                        } else if data.depositStatus == 3 {
                            user.isDeposit =  2
                        }
                        
                    } else if self.page == .dateRent {
                        if data.depositStatus == 1 {
                            user.isDayDeposit = 2
                        } else if data.depositStatus == 2 {
                            user.isDayDeposit = 1
                        } else if data.depositStatus == 3 {
                            user.isDayDeposit =  2
                        }
                    }
                    User.saveUserInfo(user)
                }


                switch self.page {
                case .timeRent?:      //分时
                    self.topTitleLabel.text = "基础押金"
                    self.topAmountLabel.text = data.basicDeposit
                    self.bottomTitleLabel.text = "已缴基础押金"
                    self.bottomAmountLabel.text = data.payeddeposit

                    switch data.depositStatus {
                    case 1:
                        self.topBtn.isHidden = true
                        self.botBtn.isHidden = false
                        self.botBtn.setTitle("退款", for: .normal)
                        self.botBtn.setTitleColor(UIColor(hexString: "bebec8"), for: .normal)
                        case 2:
                        self.topBtn.isHidden = true
                        self.botBtn.isHidden = false
                        
                        case 3:
                        self.topBtn.isHidden = true
                        self.botBtn.setTitle("退款中", for: .normal)
                        self.botBtn.setTitleColor(UIColor(hexString: "bebec8"), for: .normal)
                        
                    default:break
                    }
                    
                case .dateRent?:      //日租
                    self.topTitleLabel.text = "日租保证金"
                    self.topAmountLabel.text = data.basicDeposit
                    self.bottomTitleLabel.text = "已缴日租保证金"
                    self.bottomAmountLabel.text = data.payeddeposit
                    
                    switch data.depositStatus {
                    case 1:
                        self.topBtn.isHidden = true
                        self.botBtn.isHidden = false
                        self.botBtn.setTitle("退款", for: .normal)
                        self.botBtn.setTitleColor(UIColor(hexString: "bebec8"), for: .normal)
                    case 2:
                        self.topBtn.isHidden = true
                        self.botBtn.isHidden = true
                    case 3:
                        self.topBtn.isHidden = true
                        self.botBtn.setTitle("退款中", for: .normal)
                        self.botBtn.setTitleColor(UIColor(hexString: "bebec8"), for: .normal)
                    default:break
                    }

                case .dateReserve?:   //日租预定
                    self.topTitleLabel.text = "待缴金额"
                    self.topAmountLabel.text = data.payableDeposit
                    self.bottomTitleLabel.text = "已缴金额"
                    self.bottomAmountLabel.text = data.payeddeposit
                    //如果是从预订进来的并且是橙年用户，则有提示
                    if User.memberType() == 3 {
                        self.topConst.constant = 55.5
                    }
                    
                    switch data.depositStatus {
                    case 1:
                        self.navigationController?.popViewController(animated: true)
                    case 2:
                        self.topBtn.isHidden = false
                        self.botBtn.isHidden = true
                        if data.payableDeposit == "0.00" {
                            self.navigationController?.popViewController(animated: true)
                        }

                    case 3:break
                    default:break
                    }
                default:break
                }
            }
        }) { [weak self] message in
            JZLoading.hideLoading()
            self?.showError(message)
        }
    }
    
    @IBAction func workSpaceClick(_ sender: UIButton) {
        if sender.tag == 100 {
            //上面的按钮
            performSegue(withIdentifier: "jiaoyajinSence", sender: sender)
        } else {
            //下面的按钮
            if self.data?.depositStatus == 1 {
                //去退款
                if User.isBoundCard() {
                    self.outDeposit()
                } else {
                    alertWithCancelButton(title: "温馨提示", message: "押金退还后，您将无法继续用车，退款申请提交后押金会在7个工作日内按原支付路径退还", okTitle: "确定", cancelTitle: "取消", okStyle: .default) { [weak self] in
                        self?.outDeposit()
                    }
                }
                return
            } else if self.data?.depositStatus == 2 {
                performSegue(withIdentifier: "jiaoyajinSence", sender: sender)
            }
        }
    }
    
    @IBAction func bindCardAction(_ sender: Any) {
        if !User.isAuthentication() {
            if let user = User.info() {
                let storyBoard = UIStoryboard(name: "Attestation", bundle: nil)
                switch user.certStep {
                case 0:
                    if let vc = storyBoard.instantiateViewController(withIdentifier: "IDCardViewController") as? IDCardViewController {
                        navigationController?.pushViewController(vc, animated: true)
                    }
                    case 1:
                        if let vc = storyBoard.instantiateViewController(withIdentifier: "DrivingLicenseViewController") as? DrivingLicenseViewController {
                            navigationController?.pushViewController(vc, animated: true)
                    }
                    case 2:
                        let vc = storyBoard.instantiateViewController(withIdentifier: "ReviewAttentationSegue")
                            navigationController?.pushViewController(vc, animated: true)
                    case 3:
                        if let vc = storyBoard.instantiateViewController(withIdentifier: "RejectViewController") as? RejectViewController {
                            navigationController?.pushViewController(vc, animated: true)
                    }
                default:break
                }
            }
            
        } else {
            //判断是否绑定信用卡
            if isBoundCard == 1 || !User.isBoundCard() {
                performSegue(withIdentifier: "gotoBoundCardSence", sender: nil)
            } else {
                performSegue(withIdentifier: "gotoCardListSence", sender: nil)
            }
        }
    }
    
    //开始调用接口进行退还押金
    func outDeposit() {
        let temp = self.page == .timeRent ? 1 : 2
        JZLoading.showLoadingWith(toView: view)
        User.outUserDeposit(type: temp, success: { [weak self] in
            JZLoading.hideLoading()
            XHProgressHUD.showSuccess("退押成功")
            self?.quest()
        }) { [weak self] message in
            JZLoading.hideLoading()

            if message.code == 12001 {
                self?.alert(message: message.msg, completion: nil)
            } else if message.code == 12002 {
                self?.performSegue(withIdentifier: "returnDepositSence", sender: nil)
            } else {
                self?.showError(message.msg)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "jiaoyajinSence"{
            
            guard let btn = sender as? UIButton,let numberOfCount = data?.payableDeposit else {
                return
            }
            
            let controller = segue.destination as! PayDepositViewController
            controller.numberInfPay = Double(numberOfCount)!
            controller.needAcccountPay = 0
            controller.page = page

            if btn.tag == 100 {
                //点击了上面的支付按钮
                switch page {
                case .timeRent?:controller.workType = .payDeposit
                case .dateRent?:break
                case .dateReserve?:controller.workType = .payDayDeposit
                default:break
                }
            } else {
                //点击了下面的按钮
                controller.workType = .payDeposit
            }
        } else if segue.identifier == "returnDepositSence" {
            let temp = self.page == .timeRent ? 1 : 2
            if let vc = segue.destination as? ReturnedDepositController, let value = data?.payeddeposit {
                vc.returnMoneyNumber = value
                vc.type = temp
            }

        }
        
    }
}
