//
//  WrongDriveViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/9.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

class WrongDriveViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var data : User.falseDriveInfo?
    var index  = 1
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var leftLine : UIView!
    @IBOutlet weak var InfoView: UIView!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var rightLine : UIView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func chooseButton(_ sender: UIButton) {
        sender.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16.0)

        if sender.tag == 50 {
            leftLine.isHidden = false
            rightLine.isHidden = true
            rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
            index = 1
        } else {
            leftLine.isHidden = true
            rightLine.isHidden = false
            leftButton.titleLabel?.font = UIFont.systemFont(ofSize: 16.0)
            index = 2
        }
        //数据请求
        requestInfo(index: index)
    }
    
    func requestInfo(index : Int) {
        
        //如果切换了按钮，则先清除
        if index != index {
            data = nil
        }
        
        JZLoading.showLoadingWith(toView: self.view)
        
        User.fetchUserWrongDriveInfo(page: 1, page_size: 20, handle_flag: index, success: { [weak self] data in
            JZLoading.hideLoading()
            
            if let self = self {
                self.data = data
                UIView.removeStatusView()
                self.tableView.reloadData()
                
                if self.data?.list.count == 0{
                    UIView.showInfoViewWithEmpty(self.view, text: "暂无违章记录")
                    return
                }
            }
        }) { [weak self] message in
            JZLoading.hideLoading()
            self?.showError(message)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data?.list.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let testView2 = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 5))
        testView2.backgroundColor = UIColor(valueRGB: 0xDCDCDC, alpha: 1.0)
        return testView2
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return  10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Identifier") as! SlipInfoCell
        let model = data?.list[indexPath.section]
        if model != nil {
            cell.configItem(model: model!)
        }
        if self.index == 2 {
            cell.backgroundColor = UIColor(valueRGB: 0xfbfafa, alpha: 1.0)
            cell.topView.backgroundColor = UIColor(valueRGB: 0xfbfafa, alpha: 1.0)

            cell.scoreLabel.textColor = UIColor(valueRGB: 0x808080, alpha: 1.0)
            cell.moneyLabel.textColor = UIColor(valueRGB: 0x808080, alpha: 1.0)
            
            cell.addressLabel.textColor = UIColor(valueRGB: 0x808080, alpha: 1.0)
            cell.carTypeLabel.textColor = UIColor(valueRGB: 0x808080, alpha: 1.0)
            cell.carNumberLabel.textColor = UIColor(valueRGB: 0x808080, alpha: 1.0)
            cell.adressLabel.textColor = UIColor(valueRGB: 0x808080, alpha: 1.0)
            cell.sexLabel.textColor = UIColor(valueRGB: 0x808080, alpha: 1.0)
            cell.thingLabel.textColor = UIColor(valueRGB: 0x808080, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
            cell.topView.backgroundColor = UIColor.white

            cell.scoreLabel.textColor = UIColor(valueRGB: 0xF56C3D, alpha: 1.0)
            cell.moneyLabel.textColor = UIColor(valueRGB: 0xF56C3D, alpha: 1.0)
            
            cell.addressLabel.textColor = UIColor(valueRGB: 0x333333, alpha: 1.0)
            cell.carTypeLabel.textColor = UIColor(valueRGB: 0x333333, alpha: 1.0)
            cell.carNumberLabel.textColor = UIColor(valueRGB: 0x333333, alpha: 1.0)
            cell.adressLabel.textColor = UIColor(valueRGB: 0x333333, alpha: 1.0)
            cell.sexLabel.textColor = UIColor(valueRGB: 0x333333, alpha: 1.0)
            cell.thingLabel.textColor = UIColor(valueRGB: 0x333333, alpha: 1.0)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 137
    }

    override func viewDidLoad() {
        //首先把 菜单 和 分页视图控制器 添加到视图上
        
        tableView.register(UINib(nibName: "SlipInfoCell", bundle: nil), forCellReuseIdentifier: "Identifier")
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        
        requestInfo(index: 1)
    }
}
