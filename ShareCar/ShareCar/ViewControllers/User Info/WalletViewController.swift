//
//  WalletViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/6/29.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class WalletViewController: UIViewController {
    
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var data = ["蜜钱儿","优惠券","基础押金","日租保证金","信用卡","日租预售券"]
    var info : User.UserInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        JZLoading.showLoadingWith(toView: self.view)
        User.fetchUserInfo(success: { [weak self] info in
            JZLoading.hideLoading()
            self?.info = info
            
            //更改用户信息是否已绑定信用卡
            if let user = User.info() , let info = info{
                user.bindFlag = info.bind_flag
                User.saveUserInfo(user)
            }

            self?.tableView.isHidden = false
            self?.tableView.reloadData()
            self?.moneyLabel.text = info?.money
        }) { [weak self] message in
            JZLoading.hideLoading()
            self?.showError(message)
        }
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        func gotoPage() {
            if !User.isAuthentication() {
                if let user = User.info() {
                    let storyBoard = UIStoryboard(name: "Attestation", bundle: nil)
                    switch user.certStep {
                    case 0:
                        if let vc = storyBoard.instantiateViewController(withIdentifier: "IDCardViewController") as? IDCardViewController {
                            navigationController?.pushViewController(vc, animated: true)
                        }
                    case 1:
                        if let vc = storyBoard.instantiateViewController(withIdentifier: "DrivingLicenseViewController") as? DrivingLicenseViewController {
                            navigationController?.pushViewController(vc, animated: true)
                        }
                    case 2:
                        let vc = storyBoard.instantiateViewController(withIdentifier: "ReviewAttentationSegue")
                        navigationController?.pushViewController(vc, animated: true)
                    case 3:
                        if let vc = storyBoard.instantiateViewController(withIdentifier: "RejectViewController") as? RejectViewController {
                            navigationController?.pushViewController(vc, animated: true)
                        }
                    default:break
                    }
                }

            } else {
                let ident = info?.bind_flag == 1 ? "gotoBoundCardSence" : "cardListSence"
                performSegue(withIdentifier: ident, sender: nil)
            }
        }
        switch indexPath.row {
        case 0: break
        case 1: performSegue(withIdentifier: "couponSence", sender: nil)
        case 2,3: performSegue(withIdentifier: "gotoDepositSence", sender: indexPath.row)
        case 4:gotoPage()
        case 5: performSegue(withIdentifier: "DayQuarySence", sender: nil)
        default:break
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "gotoDepositSence" {
            if let vc = segue.destination as? DepositInfoViewController,
                let temp = sender as? Int {
                vc.page = temp == 2 ? .timeRent : .dateRent
                vc.isBoundCard = info?.bind_flag
            }
        }
    }
}

extension WalletViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! WalletTableViewCell

        cell.titleLabel?.text = data[indexPath.row]
        switch indexPath.row {
        case 0:
            cell.rightCost.constant = 15
            cell.smallimageView.isHidden = true
            cell.detailLabel?.text = info?.mi_money
            cell.detailLabel?.textColor = UIColor(hexString: "6c6c8d")
        case 1:
            if let count = info?.coupon_count {
                cell.detailLabel?.text = "\(count)张"
                if count > 0 {
                    cell.detailLabel?.textColor = UIColor(hexString: "6c6c8d")
                } else {
                    cell.detailLabel?.textColor = UIColor(hexString: "bebec8")
                }
            }
        case 2:
            if User.isBoundCard() {
                if info?.is_deposit == 1 {
                    cell.detailLabel?.textColor = UIColor(hexString: "6c6c8d")
                    cell.detailLabel?.text = "0.00"
                } else {
                    cell.detailLabel?.textColor = UIColor(hexString: "6c6c8d")
                    cell.detailLabel?.text = "已缴押金"
                }
            } else {
                if info?.is_deposit == 1 {
                    cell.detailLabel?.textColor = UIColor(hexString: "f56c3d")
                    cell.detailLabel?.text = "未缴押金"
                } else {
                    cell.detailLabel?.textColor = UIColor(hexString: "6c6c8d")
                    cell.detailLabel?.text = "已缴押金"
                }
            }
        case 3:
            cell.detailLabel?.text =  info?.daily_deposit
        case 4:cell.detailLabel.text = info?.bind_flag == 1 ? "绑卡免押金用车":"查看"
            
        case 5:
            if let count = info?.day_coupon_count {
                cell.detailLabel?.text = "\(count)张"
                if count > 0 {
                    cell.detailLabel?.textColor = UIColor(hexString: "6c6c8d")
                } else {
                    cell.detailLabel?.textColor = UIColor(hexString: "bebec8")
                }
            }

        default:break
        }
        return cell
    }
}
