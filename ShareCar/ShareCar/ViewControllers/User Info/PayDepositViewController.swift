

//
//  PayDepositViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/5.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class PayDepositViewController: UIViewController{
    
    
    enum JZworkType : Int,Codable {
        case payDeposit = 0            //支付时租押金
        case payCard    = 1            //支付月卡
        case payOrder   = 2            //支付订单
        case payAccount = 3            //账户充值
        case payDebtorder = 4          //支付欠款订单
        case payDayDeposit = 5         //支付日租押金
    }
    
    enum JZCardType : Int,Codable {
        case monthCard  = 0     //月卡
        case yearCard   = 1     //年卡
    }
    
    var page: DepositInfoViewController.Page!
    var numberInfPay = 0.0      //需要支付的金额
    var orderId : String!       //需要支付的订单id
    var needAcccountPay: Int?   //上个页面传值判断是否需要账户支付  0: 否  1: 是
    
    @IBOutlet weak var payTable: UITableView!
    @IBOutlet weak var depositCountLabel: UILabel!
    @IBOutlet weak var payTableHeight: NSLayoutConstraint!
    
    @IBOutlet var paySwith: PaySwith!
    var workType = JZworkType.payDeposit   //默认是支付押金
    var cardType = JZCardType.monthCard    //默认是月卡
    
    //MARK: - system function
    
    override func viewDidLoad() {
        super.viewDidLoad()
        depositCountLabel.text = "\(String(numberInfPay))"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if needAcccountPay == 0 {
            paySwith.balance = false
            payTableHeight.constant = 100
            payTable.reloadData()
        }
        paySwith.addObserve { [weak self] payment, success in
            
            switch payment {
            case .aliPay:
                
                if success {
                    self?.changUserInfo()
                } else {
                    XHProgressHUD.showError("你取消了支付")
                }
            case .wePay:
                if success {
                    self?.changUserInfo()
                } else {
                    XHProgressHUD.showError("你已取消支付")
                }
            default:break
            }
        }
    }
    
    //MARK: - custom function
    
    //修改用户信息
    func changUserInfo() {
        switch workType {
        case .payDeposit:
            Pay.changeUserDeposit(status: Pay.UserDepositType.deposited)  //支付押金的时候升级用户信息为已支付押金用户
        case .payCard:
            changeUserInfo(cardType.rawValue)             //购买成功之后修改本地缓存为蜜月用户
            slipViewController?.menuViewController?.refresh()
        case .payDayDeposit:
            Pay.changeUserDayDeposit(status: Pay.UserDepositType.deposited)
        default: break
        }
        XHProgressHUD.showSuccess("支付成功")
        navigationController?.popViewController(animated: true)
    }

    //支付一定的金额
    func payUserDeposit(_ type:DepositInfoViewController.Page) {
        //模拟支付
        guard let payment = paySwith.selected else {
            XHProgressHUD.showError("请选择支付方式")
            return
        }
        
        User.payDeposit(deposit: Float(numberInfPay),
                        payWay: payment.rawValue,
                        type:page.rawValue,
                        success: { [weak self] data in
                            if let Info = data, let self = self {
                                self.payOrder(Info.pay_str)
                            }
        }) { error in
            if error.code == 2011 {
                XHProgressHUD.showSuccess(error.msg)
                self.navigationController?.popViewController(animated: true)
            } else {
                XHProgressHUD.showError(error.msg)
            }
        }
    }
    
    //支付订单
    func payUnfinishOrder() {
        
        guard let payment = paySwith.selected else {
            XHProgressHUD.showError("请选择支付方式")
            return
        }

        Order.payUnfinishOrder(orderId: orderId, payment: payment.rawValue, success: { [weak self] data in
            if let Info = data{
                self?.payOrder(Info.payStr)
            }
        }) { (message) in
            XHProgressHUD.showError(message.msg)
        }
    }
    
    //支付会员卡
    func payCreditCard() {
        
        guard let payment = paySwith.selected else {
            XHProgressHUD.showError("请选择支付方式")
            return
        }
        
        User.PayMonthandYearCard(member_type: cardType.rawValue + 1, payment: payment.rawValue, success: { [weak self] data in
            if let Info = data{
                self?.payOrder(Info.pay_str)
            }
        }) { [weak self] message in
            self?.showError(message)
        }
    }

    //根据info信息吊起不同的支付
    func payOrder(_ payStr: String) {
        
        guard let payment = paySwith.selected else {
            XHProgressHUD.showError("请选择支付方式")
            return
        }

        switch payment {
        case .wePay:   Pay.wechatPay(payStr: payStr)
        case .aliPay:   Pay.zhifuPay(payStr: payStr)
        case .balance:
                //余额支付成功
                XHProgressHUD.showSuccess("余额支付成功")
                //根据支付业务修改用户本地信息
                switch workType {
                case .payDeposit: break  //因为写死了押金不能用余额支付
                case .payCard:
                    changeUserInfo(cardType.rawValue)             //购买成功之后修改本地缓存为蜜月用户
                    slipViewController?.menuViewController?.refresh()
                    
                default: break
                }
            navigationController?.popViewController(animated: true)
        case .ticket:
            break
        }
    }
    
    //修改购买会员卡以后的缓存信息
    func changeUserInfo(_ tag: Int ) {
        if let user = User.info() {
            user.memberType = tag + 2
            User.saveUserInfo(user)
        }
    }

    //支付欠款订单
    func payOrderDebt() {
        guard let payment = paySwith.selected else {
            XHProgressHUD.showError("请选择支付方式")
            return
        }
        
        Order.payOrderDebt(receipt_sn: orderId, payment: payment.rawValue, success: { [weak self] data in
            if let Info = data{
                self?.payOrder(Info.pay_str)
            }
        }) { (message) in
            XHProgressHUD.showError(message.msg)
        }
    }
    
    //MARK: - IBAction
    //立即支付
    @IBAction func pay(_ sender : UIControl){
        if Float(numberInfPay) > 0.0 {
            switch workType {
            case .payDeposit: payUserDeposit(page)
            case .payCard:    payCreditCard()
            case .payOrder:   payUnfinishOrder()
            case .payAccount: p("账户充值")
            case .payDebtorder:payOrderDebt()
            case .payDayDeposit:payUserDeposit(page)
            }
        } else {
            showError("没有需要支付的金额")
        }
    }
}
