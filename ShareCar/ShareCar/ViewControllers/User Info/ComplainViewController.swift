//
//  ComplainViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/10.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class ComplainViewController: UIViewController, UITextViewDelegate {
    
    var data : User.orderInfo?
    var dayData: User.OrderDetailDayInfo?
    var orderId : String!
    
    @IBOutlet weak var height: NSLayoutConstraint!
    
    @IBOutlet weak var topOneLabel: UILabel!
    @IBOutlet weak var topTwoLabel: UILabel!
    @IBOutlet weak var topThreeLabel: UILabel!
    @IBOutlet weak var topFourLabel: UILabel!
    
    @IBOutlet weak var orderSnLabel: UILabel!
    
    @IBOutlet weak var bottomOneLabel: UILabel!
    @IBOutlet weak var bottomTwoLabel: UILabel!
    @IBOutlet weak var bottomThreeLabel: UILabel!
    @IBOutlet weak var bottomFourLabel: UILabel!
    
    @IBOutlet weak var carImageView: UIImageView!
    
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var InfoView: UIView!
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var texField: UITextField!
    
    var index : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        height.constant = 7
        InfoView.isHidden = true
        setup()
    }
    
    @IBAction func chooseButton(_ sender: UIButton) {
        
        if sender.tag == 80 {
            leftButton.isSelected = true
            rightButton.isSelected = false
            index = 1
        } else {
            index = 2
            leftButton.isSelected = false
            rightButton.isSelected = true
        }
        update(status: index)
    }
    
    func update(status: Int) {
        
        view.setNeedsLayout()
        UIView.animate(withDuration: 0.25, animations: { [weak self] in
            if status == 1 {
                self?.height.constant = 7
                self?.InfoView.isHidden = true
            } else {
                self?.height.constant = 64
                self?.InfoView.isHidden = false
            }
            self?.view.layoutIfNeeded()
            
        }) { (status) in
            
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        tipLabel.isHidden = textView.text.count == 0 ? false : true
    }
    
    func setup() {
        
        if let data = data {
            topOneLabel.text = data.plateNumber
            topTwoLabel.text = data.carTypeName
            orderSnLabel.text = data.orderSn
        }
        
        if let data = dayData {
            topOneLabel.text = data.plateNumber
            topTwoLabel.text = data.carTypeName
            orderSnLabel.text = data.orderSn
        }
    }
    
    @IBAction func commitContent(_ sender: UIButton) {
        if textView.text == nil {
            showError("请输入申诉内容")
            return
        }
        
        if index == 2 && (texField.text?.isEmpty)!{
            showError("请输入申诉金额")
            return
        }
        
        User.UserComplainOrder(orderId: orderId, complain_type: index, complain_content: textView.text, complain_money: String(30), success: { [weak self] status in
            if self?.index == 1 {
                XHProgressHUD.showSuccess("投诉成功")
                self?.navigationController?.popViewController(animated: true)
            } else {
                XHProgressHUD.showSuccess("申诉成功")
            }
        }) { (message) in
            XHProgressHUD.showError(message.msg)
        }
    }
}

