//
//  BoundCardInfoController.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2019/1/4.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class BoundCardInfoController: UIViewController {
    
    
    var bankInfo: User.BankInfo!
    var bankNumber: String!
    @IBOutlet var tipView: UIView!
    @IBOutlet weak var codeIphoneTextField: UITextField!
    @IBOutlet weak var iphoneTextField: UITextField!
    @IBOutlet weak var durationTextField: UITextField!
    @IBOutlet weak var safeCodeTextField: UITextField!
    @IBOutlet weak var identityLabel: UILabel!
    @IBOutlet weak var guestNameLabel: UILabel!
    @IBOutlet weak var bankInfoLabel: UILabel!
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var bankIconImageView: UIImageView!
    @IBOutlet weak var factImageView: UIImageView!
    
    //MARK: --System method
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        func controster(){
            bankNameLabel.text = bankInfo.bankName
            bankIconImageView.kf.setImage(with: URL(string: kImageServerName + "/" + bankInfo.bankIcon))
            guestNameLabel.text = bankInfo.userName
            identityLabel.text = bankInfo.identifyNumber
            bankInfoLabel.text = "尾号\(bankNumber.suffix(4))"
        }
        controster()
    }
    
    //MARK: --IBAction method
    @IBAction func makeCode(_ sender: GSCountdownButton) {
        if !condifition() {return}
        JZLoading.showLoadingWith(toView: view)
        User.boundCardSendCode(creditNumber: bankNumber, safeCode: safeCodeTextField.text!, expireTime: durationTextField.text!, telephone: iphoneTextField.text!, success: { [weak self] in
            JZLoading.hideLoading()
            sender.maxSecond = 60
            sender.countdown = true
            
            self?.safeCodeTextField.isEnabled = false
            self?.durationTextField.isEnabled = false
            self?.iphoneTextField.isEnabled = false
            
        }) { [weak self] error in
            JZLoading.hideLoading()
            self?.showError(error.msg)
        }
    }
    
    @IBAction func touchTipView(_ sender: UITapGestureRecognizer) {
        tipView.removeFromSuperview()
    }
    
    @IBAction func agree(_ sender: UIButton) {
        if !condifition() {return}
        guard codeIphoneTextField.text != nil else {
            showError("验证码不能为空，请输入验证码（2205）")
            return
        }
        
        guard codeIphoneTextField.text?.count == 6 else {
            showError("短信验证码错误，请重新输入（2210）")
            return
        }
        
        JZLoading.showLoadingWith(toView: view)
        User.boundingCard(bankId: bankInfo.bankId, creditNumber: bankNumber, safeCode: safeCodeTextField.text!, expireTime: durationTextField.text!, telephone: iphoneTextField.text!, checkCode: codeIphoneTextField.text!, success: { [weak self] in
            JZLoading.hideLoading()
            //更改用户信息为已绑定信用卡
            if let user = User.info() {
                user.bindFlag = 2
                User.saveUserInfo(user)
            }
            self?.performSegue(withIdentifier: "agresProtocolSence", sender: nil)
        }) { [weak self] error in
            JZLoading.hideLoading()
            self?.codeIphoneTextField.text = ""
            self?.showError(error.msg)
        }
        
    }
    
    @IBAction func help(_ sender: UIButton) {
        factImageView.image = sender.tag == 100 ? UIImage(named: "safeImage") : UIImage(named: "expactfactimage")
        UIApplication.shared.keyWindow?.addSubview(tipView)
        tipView.snp.makeConstraints { make in
            make.top.left.bottom.right.equalTo(0)
        }

    }
    
    func condifition() -> Bool {
        guard safeCodeTextField.text != "" else {
            showError("安全码不能为空，请输入安全码（2203）")
            return false
        }
        
        guard safeCodeTextField.text?.count == 3 else {
            showError("安全码格式错误，请重新输入（2207）")
            return false
        }
        guard durationTextField.text != "" else {
            showError("有效期不能为空，请输入有效期（2204）")
            return false
        }
        guard durationTextField.text?.count == 4 else {
            showError("有效期格式错误，请重新输入（2208）")
            return false
        }
        guard iphoneTextField.text != "" else {
            showError("手机号不能为空，请输入手机号（2206）")
            return false
        }
        guard iphoneTextField.text?.count == 11 else {
            showError("手机号格式错误，请重新输入（2209）")
            return false
        }
        return true
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
