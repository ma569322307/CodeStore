//
//  CardListViewController.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2019/1/7.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class CardListViewController: UIViewController {
    var data = [User.CardList.CardInfo]()

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        JZLoading.showLoadingWith(toView: view)
        User.makeBoundedCardList(flag: "own", success: { [weak self] data in
            JZLoading.hideLoading()
            if let data = data?.list {
                self?.data = data
                self?.tableView.reloadData()
            }
            self?.tableView.reloadData()
        }) { [weak self] error in
            JZLoading.hideLoading()
            self?.showError(error.msg)
        }
    }
    
    //MARK: --IBAction method
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CardListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CardCell
        cell.config(item: self.data[indexPath.row])
        return cell
    }
    
}
