//
//  BoundCardFinishController.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2019/1/7.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class BoundCardFinishController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func finish(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
