//
//  NavControllerViewController.swift
//  ShareCar
//
//  Created by lishuai on 2018/7/16.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit

class NavigationController: UINavigationController, UIGestureRecognizerDelegate {

    @IBInspectable var backButtonImage: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(valueRGB: 0xf3f3f3, alpha: 0.5)

        interactivePopGestureRecognizer?.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func popself() {
        self.popViewController(animated: true)
    }
    
    func createBackButton() -> UIBarButtonItem {
        return UIBarButtonItem(image: backButtonImage?.withRenderingMode(.alwaysOriginal),
                               style: .plain,
                               target: self,
                               action: #selector(popself))
    }
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        super.pushViewController(viewController, animated: animated)
        
        if viewController.navigationItem.leftBarButtonItem == nil && viewControllers.count > 1 {
            viewController.navigationItem.leftBarButtonItem = createBackButton()
        }
    }
}
