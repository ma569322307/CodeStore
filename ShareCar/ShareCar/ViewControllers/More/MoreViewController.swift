//
//  MoreViewController.swift
//  ShareCar
//
//  Created by lishuai on 2018/10/29.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class MoreViewController: UITableViewController {

    struct Cell {
        let type: String
        let title: String
        let segue: String
        let url: String?
    }
    
    var order: Order.Detail?
    
    var data = [Cell]() {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if order?.orderType == 1 { //分时
            let cell = Cell(type: "cell1", title: "中途加油", segue: "addOilSegue", url: nil)
            data.append(cell)
        }

        
        let guaranteeCell = Cell(type: "cell2", title: "行驶证照片", segue: "drivingSence", url: nil)
        data.append(guaranteeCell)
        
        let drivingCell = Cell(type: "cell2", title: "保单照片", segue: "guaranteeSence", url: nil)
        data.append(drivingCell)

        
        Guide.getGuide(guideType: 2, page: 1, pageSize: 100, success: { [weak self] data in
            if let d = data?.list {
                for i in d {
                    let cell = Cell(type: "cell2", title: i.title , segue: "webViewSegue", url:i.url)
                    self?.data.append(cell)
                }
            }
        }) { [weak self] message in
            self?.showError(message)
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = data[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.type, for: indexPath)
        cell.textLabel?.text = item.title
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = data[indexPath.row]
        performSegue(withIdentifier: item.segue, sender: item.url)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addOilSegue" {
            if let vc = segue.destination as? AddOilViewController {
                vc.orderId = order?.orderId
            }
        } else if segue.identifier == "guaranteeSence" {
            if let vc = segue.destination as? GuaranteeViewController {
                vc.orderId = order?.orderId
            }
        }  else if segue.identifier == "drivingSence" {
            if let vc = segue.destination as? DrivingBookViewController {
                vc.orderId = order?.orderId
            }
        } else if segue.identifier == "webViewSegue" {
            if let nav = segue.destination as? UINavigationController,
                let vc = nav.viewControllers.first as? WebViewController,
                let url = sender as? String {
                vc.page = Page.URL(url)
            }
        }
    }
}
