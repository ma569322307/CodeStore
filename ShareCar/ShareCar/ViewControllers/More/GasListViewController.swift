//
//  GasListViewController.swift
//  ShareCar
//
//  Created by lishuai on 2018/6/30.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit

class GasListViewController: UIViewController {
    
    @IBOutlet weak var gasTableView: UITableView!
    
    weak var delegate: AddOilViewController?
    
    private var data = [Order.GasList.Gas]() {
        didSet {
            gasTableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Order.gasList(success: { [weak self] data in
            if let d = data {
                self?.data = d
            }
        }) { error in
            p(error)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension GasListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let item = data[indexPath.row]
        cell.textLabel?.text = item.gasName
        return cell
    }
}

extension GasListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = data[indexPath.row]
        
        self.delegate?.gasId = item.gasId
        self.delegate?.gasNameLabel.text = item.gasName
         self.navigationController?.popViewController(animated: true)
    }
}
