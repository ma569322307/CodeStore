//
//  DrivingBookViewController.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/11/7.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class DrivingBookViewController: UIViewController {
    
    var orderId: String!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet var tipview: UIView!
    
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var otherImageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        User.certificate(orderId: orderId, success: { [weak self] data in
            if let data = data {
                self?.setup(info: data)
            }
        }) { [weak self] error in
            self?.showError(error.msg)
        }
    }
    
    func setup(info: User.Guarantee) {
        if info.certPhotoFlag == 1 {
            stackView.isHidden = true
            view.addSubview(tipview)
        } else {
            mainImageView.kf.setImage(with: URL(string: info.license1ImagePath))
            otherImageView.kf.setImage(with: URL(string: info.license2ImagePath))
        }
    }
}
