//
//  define.swift
//  ShareCar
//
//  Created by lishuai on 2018/6/21.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

//服务器地址
#if DEBUG
//测试
let kServerName = "http://timerenttest.9zcap.com/api/"
let kImageServerName = "http://imgtest.9zcap.com"

//开发
//let kServerName = "http://timerenttest.9zfs.com/api/"
//let kImageServerName = "http://imgtest.9zcap.com"

//http://timerenttest.9zfs.com


//生产
//let kServerName = "http://timerent.9zcap.com/api/"
//let kImageServerName = "http://img.9zcap.com"

//预生产
//let kServerName = "http://prtimerent.9zcap.com/api/"
//let kImageServerName = "http://img.9zcap.com"

#else
let kServerName = "http://timerent.9zcap.com/api/"
let kImageServerName = "http://img.9zcap.com"
#endif

//400电话
let kCallCenter = URL(string: "tel:4001099909")

//高德key
let kAMapKey = "1aa2b04fcd9d188bd8d4ae3b813c9386"

//接口私有key
let kApiPrivateKey = "0f977b6090bc11e89de47ef7fbe91ebc"

let kMaxSizeKBytes = 100.0

let kScreenWidth = UIScreen.main.bounds.size.width
let kScreenHeight = UIScreen.main.bounds.size.height
let userInfoStory = UIStoryboard.init(name: "UserInfo", bundle: nil)

let kUserInfo = "user_info"

func p<N>(_ itme: N, file: String = #file, fun: String = #function, line: Int = #line) {
    #if DEBUG
    print("----------------------")
    print(file, ":", fun, ":", line, "\n", itme)
    print("----------------------")
    #endif
}
