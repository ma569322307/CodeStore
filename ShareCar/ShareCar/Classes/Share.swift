//
//  Share.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/8/30.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation


class Share {
    
    struct shareModel: Codable {
        let title: String
        let description: String
        let imageName: String
        let url: String
    }
    
    //分享功能
    static func share(_ model: shareModel,_ type: WXScene = WXSceneTimeline) {
        let message = WXMediaMessage()
        message.title = model.title
        message.description = model.description
        message.setThumbImage(UIImage(named: model.imageName))
        
        let webpageOject = WXWebpageObject()
        webpageOject.webpageUrl = model.url
        message.mediaObject = webpageOject
        
        let req = SendMessageToWXReq()
        req.bText = false
        req.message = message
        req.scene = Int32(Float(type.rawValue))
        WXApi.send(req)
        
    }
}
