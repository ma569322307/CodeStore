//
//  SegueHandlerType.swift
//  ShareCar
//
//  Created by 李帅 on 2019/1/21.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit
import Foundation

protocol SegueHandlerType {
    associatedtype SegueIdentifier: RawRepresentable
        where Self: UIViewController, SegueIdentifier.RawValue == String
}

extension SegueHandlerType {

    func performSegue(_ segueIdentifier: SegueIdentifier, sender: Any?) {
        performSegue(withIdentifier: segueIdentifier.rawValue, sender: sender)
    }

    func segueIdentifier(forSegue segue: UIStoryboardSegue) -> SegueIdentifier {
        guard let identifier = segue.identifier,
            let segueIdentifier = SegueIdentifier(rawValue: identifier) else {
                fatalError("Invalid segue identifier \(String(describing: segue.identifier)).") }
        return segueIdentifier
    }
}
