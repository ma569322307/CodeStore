//
//  PaySwith.swift
//  ShareCar
//
//  Created by 李帅 on 2018/12/18.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import Foundation

class PaySwith: NSObject {
    
    var balance: Bool = true
    
    @IBInspectable var ticket: Bool = false
    
    var textField: UITextField?
    
    private var paySwithData: [Order.PaySwith.Method] {
        var data = [Order.PaySwith.Method]()
        
        if balance {
            data.append(Order.PaySwith.Method(payMethod: Order.Pay.Payment.balance))
        }
        
        data.append(Order.PaySwith.Method(payMethod: Order.Pay.Payment.wePay))
        data.append(Order.PaySwith.Method(payMethod: Order.Pay.Payment.aliPay))
        
        if ticket {
            data.append(Order.PaySwith.Method(payMethod: Order.Pay.Payment.ticket))
        }
        
        return data
    }
    
    var selected: Order.Pay.Payment? = nil {
        didSet {
            guard let delegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
            
            guard let payment = selected else {
                return
            }
            
            delegate.payblock { [weak self] response in
                
                switch payment {
                case .aliPay:
                    if let value = response as? [AnyHashable : AnyObject] {
                        let code = value["resultStatus"] as? String
                        if code == "9000" {
                            self?.block?(.aliPay, true)
                        } else {
                            self?.block?(.aliPay, false)
                        }
                    }
                case .wePay:
                    if let model = response as? BaseResp {
                        if model.errCode == -2 {
                            self?.block?(.wePay, false)
                        } else {
                            self?.block?(.wePay, true)
                        }
                    }
                default:
                    break
                }
            }
        }
    }
    
    private var block: ((Order.Pay.Payment, Bool) -> ())?
    
    func addObserve(completion: @escaping (Order.Pay.Payment, Bool) -> ()) {
        block = completion
    }
}

extension PaySwith: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paySwithData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = paySwithData[indexPath.row]
        
        let cell: UITableViewCell
        
        if item.payMethod == .ticket {
            cell = tableView.dequeueReusableCell(withIdentifier: "payCell2", for: indexPath)
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "payCell", for: indexPath)
        }
        
        if let cell = cell as? PayMethodCell {
            cell.type = item.payMethod
        }
        
        return cell
    }
}

extension PaySwith: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? PayMethodCell {
            selected = cell.type
            if cell.type == .ticket,
                let c = cell as? PayMethodCell2 {
                textField = c.textField
            } else {
                textField = nil
            }
        }
    }
}
