//
//  ParkingAnnotation.swift
//  ShareCar
//
//  Created by lishuai on 2018/6/25.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit
import MAMapKit

class ParkingAnnotation: MAPointAnnotation {
    var id: String
    var carNum: Int
    var parkingLat: Double?
    var parkingLon: Double?
    var parkingDistance: Int?
    var parkName: String?
    var spaceCount: Int?
    var signType: Order.RecommendReturnList.SignType?
    var fenceFlag: Int?
    var radius: String?
    var coordinates: [[String]]?
    
    var isFirst = false
    
    init(id: String, carNum: Int) {
        self.id = id
        self.carNum = carNum
        
        
        super.init()
    }
}
