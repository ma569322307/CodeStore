//
//  CarAnnotation.swift
//  ShareCar
//
//  Created by lishuai on 2018/6/25.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit
import MAMapKit

class CarAnnotation: MAPointAnnotation {
    var id: String
    var isFirst = false
    var icon: Int
    
    init(id: String, icon: Int) {
        self.id = id
        self.icon = icon
        super.init()
    }
}
