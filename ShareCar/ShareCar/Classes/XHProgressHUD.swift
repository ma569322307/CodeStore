//
//  XHProgressHUD.swift
//  XHBaseProject
//
//  Created by xh on 2017/4/26.
//  Copyright © 2017年 xh. All rights reserved.
//

import UIKit
import MBProgressHUD

class XHProgressHUD: MBProgressHUD {

    class func showText(text: String, icon: String? = nil) {
        let view = viewWithShow()
        
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.numberOfLines = 0
        hud.label.text = text
        
        if let icon = icon {
            let img = UIImage(named: "MBProgressHUD.bundle/\(icon)")
            hud.customView = UIImageView(image: img)
        }
        
        hud.mode = MBProgressHUDMode.customView
        hud.removeFromSuperViewOnHide = true
        
        hud.hide(animated: true, afterDelay: 1.5)
    }
    
    class func viewWithShow() -> UIView {
        var window = UIApplication.shared.keyWindow
        if window?.windowLevel != UIWindowLevelNormal {
            let windowArray = UIApplication.shared.windows
            
            for tempWin in windowArray {
                if tempWin.windowLevel == UIWindowLevelNormal {
                    window = tempWin;
                    break
                }
            }
            
        }
        window?.backgroundColor = UIColor.black
        return window!
    }
    
    class func showStatusInfo(_ info: String = "") {
        let view = viewWithShow()
        let hud = MBProgressHUD.showAdded(to: view, animated: true)
        hud.label.text = info
    }
    
    class func dismiss() {
        let view = viewWithShow()
        MBProgressHUD.hide(for: view, animated: true)
        
    }
    
    class func showSuccess(_ status: String) {
        showText(text: status, icon: "success.png")
    }
    
    class func showError(_ status: String) {
        showText(text: status, icon: "error.png")
    }

}
