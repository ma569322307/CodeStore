//
//  LoadingTitle.swift
//  ShareCar
//
//  Created by 李帅 on 2019/1/22.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import Foundation
import SnapKit

class LoadingTitle {
    
    private let timer = DispatchSource.makeTimerSource()
    
    private var loadingView: UIView!
    private var imageView: UIImageView!
    
    let t = ["安全靠着你我他，和谐交通靠大家",
             "你争我抢道路窄，互谦互让心路宽",
             "与文明一起上路，伴平安一起回家",
             "人人需要文明交通，交通需要人人文明",
             "用礼让传递文明，用安全构筑和谐",
             "出行因礼让而畅通，道路因畅通而和谐",
             "一路文明一路情，用安全构筑和谐",
             "你不抢行我不争道，文明行车和谐有道",
             "遵守交通法规，关爱生命旅程",
             "安全斑马线，幸福两相连",
             "绿灯可以再等，生命不可重来",
             "带十分小心上路，携一份平安回家",
             "出行多点小心，家人少点担心",
             "礼让心情好，争抢危害多",
             "不闯生命红灯，点亮幸福绿灯",
             "文明处处是美德，安全时时伴君行",
             "礼让是一种修养，文明是一道风景",
             "冒险是事故之根，谨慎为安全之本",
             "交通安全时时刻刻，平安和谐岁岁年年",
             "文明与畅通时时相伴，平安与和谐息息相关",
             "文明出行每一步，安全相伴每一天",
             "你我多一秒等待，家人少一分担心",
             "你礼我让，心宽路畅",
             "礼让一小步，文明一大步",
             "文明在于一言一行，安全源于一点一滴",
             "交通安全记心中",
             "遵守交通信号，遵守交通标志，遵守交通标线",
             "道路千万条，安全第一条",
             "各行其道，安全可靠",
             "退一步心顺路宽都平安，抢半步车挤人怨招祸端",
             "一身安危系全家，全家幸福系一人",
             "创优良交通秩序，闪精神文明之光",
             "交通安全系万家，平平安安是幸福",
             "安全开车是大事，文明走路非小节",
             "多一点谨慎少一份祸，少一份麻痹多一份安",
             "侥幸躲过千次，出事就在一次",
             "红灯常在心中亮，绿灯才能伴一生",
             "告别一切不文明交通行为",
             "警惕保安全，麻痹起祸端",
             "树立安全第一的思想，落实预防为主的方针",
             "安全来自警惕，事故出于麻痹",
             "多一分麻痹，多一分危险多一些谨慎，多一些安全",
             "还车时，请拔出车辆钥匙",
             "还车前，请确认车辆所有灯光关闭",
             "车辆没有自动折叠后视镜功能，如被折叠需要手动打开",
             "放完手刹再松脚刹",
             "车内禁止吸烟",
             "请保持车内清洁",
             "请在P档还车",
             "踩刹车拧钥匙，着车熄火都方便",
             "所有车辆配有倒车雷达",
             "部分车辆配置了倒车影像功能",
             "天黑请开车灯",
             "所有车辆装有ABS防抱死系统，紧急制动时一定要用车踩刹车",
             "行驶过程中，车上人员都要系好安全带",
             "请不要让儿童坐在副驾驶座位上",
             "请不要将车辆交由其它人驾驶",
             "请爱惜车辆，不要人为破坏车上部件",
             "车辆行驶中禁止安全带接触儿童的颈部或脸部",
             "开门后后备箱功能不会立即开启，需要您耐心等待几秒钟",
             "为方便他人使用，请不要将钥匙带离车辆",
             "油表在仪表盘左下方，E为空，F为满",
             "车灯控制在方向盘左手边旋转手柄端",
             "后备箱按钮位于方向盘左侧空调出风口下方",
             "上车后，请关注油量，如加油灯亮起，请就近加油或联系客服",
             "驾驶位座椅下方手柄，可以调整座椅前后位置",
             "门窗开关位于驾驶位左手边车窗下",
             "驾驶位座椅左侧可以调节座椅前后角度",
             "购买时长包，实惠又方便",
             "每天取消次数有上限，还请确认好再取消",
             "临时停车，请使用中途锁车功能",
             "看到车辆，APP上却看不到？请尝试搜车牌功能",
             "发现车辆有问题，可以使用故障上报功能，感谢配合",
             "用车有疑问，可以先查询用户指南",
             "车辆没打开，不要慌，可以稍等5秒再尝试"]
    
    private let label = UILabel()
    
    init() {
        timer.setEventHandler {
            DispatchQueue.main.async { [weak self] in
                self?.label.text = self?.t.randomElement()
            }
        }

        timer.schedule(deadline: .now() + .seconds(2), repeating: 2.5)
    }
    
    deinit {
        timer.resume()
        timer.cancel()
    }
    
    func show() {
        let maskView = UIView(frame: (UIApplication.shared.keyWindow?.bounds)!)
        maskView.backgroundColor = UIColor(hexString: "#00000080")
        //赋值全局变量
        loadingView = maskView

        //添加的桌面上
        UIApplication.shared.keyWindow?.addSubview(loadingView)
        
        let size = CGSize(width: 100, height: 100)
        
        let blackView = UIView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        blackView.backgroundColor = UIColor.clear
        blackView.layer.cornerRadius = 10
        //放到正中间
        blackView.center = loadingView.center
        loadingView.addSubview(blackView)
        
        imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.frame = blackView.bounds
        
        let source = getImages()
        imageView.animationImages = source.images
        imageView.animationDuration = source.duration
        imageView.animationRepeatCount = 300
        imageView.startAnimating()
        blackView.addSubview(imageView)
        
        label.text = "正在开门中，请稍后"
        label.textAlignment = .center
        label.textColor = .white
        label.numberOfLines = 0
        loadingView.addSubview(label)

        label.snp.makeConstraints { make in
            make.left.right.equalTo(10)
            make.top.equalTo(imageView.snp.bottom)
        }
        
        timer.resume()
    }
    
    func hide() {
        timer.suspend()
        loadingView?.removeFromSuperview()
    }
    
    private func getImages() -> (images:[UIImage], duration:TimeInterval) {
        
        var images = [UIImage]()
        
        for i in 1...15 {
            images.append(UIImage(named: "\(i)")!)
        }
        return (images, 1.0)
    }
}
