//
//  EmptyDataStatusInfo.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/8/11.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

class EmptyDataStatusInfo: UIView {
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
}
