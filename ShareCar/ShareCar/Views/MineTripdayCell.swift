
//
//  MineTripdayCell.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2018/12/6.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class MineTripdayCell: UITableViewCell {

    @IBOutlet weak var carphotoImageView: UIImageView!
    @IBOutlet weak var endTimeLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var carNumberLabel: UILabel!
    @IBOutlet weak var ordersnLabel: UILabel!
    @IBOutlet weak var statusTitleLabel: UILabel!
    @IBOutlet weak var createTimeLabel: UILabel!
    var item: User.TripList.TripInfo!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setItem(item: User.TripList.TripInfo) {
        createTimeLabel.text = item.created
        ordersnLabel.text = item.ordersn
        carNumberLabel.text = item.plateNumber
        carTypeLabel.text = item.carTypeName
        
        if let beginTime = item.beginTime, let endTime = item.endTime {
            startTimeLabel.text = "起:\(Date.timeStampToString(timeStamp: beginTime))"
            endTimeLabel.text = "终:\(Date.timeStampToString(timeStamp: endTime))"
        }
        let imageUrl = URL(string: kImageServerName + item.imagePath)
        self.carphotoImageView.kf.setImage(with: imageUrl)
        
        var text = ""
        switch item.status {
        case .reserve:   text = "已预定"
        case .cancel:   text = item.payStatus == .unFinish ? "已取消待支付" : "已取消"
        case .using:   text = "使用中"
        case .confrimPay,.confrimReturnCar:
            switch item.isNeedPay {
            case .needPay?, .unNeedPay?: text = "待支付"
            case .refund?: text = "待退款"
            case .none: text = "异常"
                
            }
        case .finished:   text = "已支付"
        }
        self.statusTitleLabel.text = item.forceFlag == .forcing ? "待结单" :
                                     item.debtFlag == .debted ? "欠款中" :  text

    }
    
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
