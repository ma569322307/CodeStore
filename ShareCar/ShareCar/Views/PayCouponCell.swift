//
//  PayCouponCell.swift
//  ShareCar
//
//  Created by lishuai on 2018/7/12.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit

class PayCouponCell: UITableViewCell {

    @IBOutlet weak var couponNameLabel: UILabel!
    @IBOutlet weak var expireTimeLabel: UILabel!
    @IBOutlet weak var briefLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var bg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            borderView.layer.borderColor = UIColor(hexString: "ffa329")?.cgColor
            borderView.backgroundColor = UIColor(hexString: "ffffe6")
            bg.image = UIImage(named: "cell_bg")
        } else {
            borderView.layer.borderColor = UIColor(hexString: "8182a3")?.cgColor
            borderView.backgroundColor = UIColor(hexString: "ffffff")
            bg.image = UIImage(named: "cell_bg2")
        }
    }

}
