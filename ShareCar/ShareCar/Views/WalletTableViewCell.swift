//
//  WalletTableViewCell.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/8.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class WalletTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var rightCost: NSLayoutConstraint!
    @IBOutlet weak var smallimageView: UIImageView!
    @IBOutlet weak var detailLabel: UILabel!    
}
