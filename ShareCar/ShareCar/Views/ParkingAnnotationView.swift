//
//  ParkingAnnotationView.swift
//  ShareCar
//
//  Created by lishuai on 2018/7/16.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit
import MAMapKit

class ParkingAnnotationView: MAAnnotationView {

    var num: Int
    
    init(annotation: MAAnnotation!, reuseIdentifier: String!, num: Int) {
        self.num = num
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier + String(num))
        self.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let image = UIImage(named: "parking_ann")
        image?.draw(at: CGPoint(x: 0, y: 0))
        
        let textStyle = NSMutableParagraphStyle()
        textStyle.alignment = NSTextAlignment.center
        
        let dict = [NSAttributedStringKey.foregroundColor: UIColor.white,
                    NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14),
                    NSAttributedStringKey.paragraphStyle: textStyle]
        "\(self.num)".draw(in: CGRect(x: 0, y: 10, width: 34, height: 14), withAttributes: dict)
    }
}
