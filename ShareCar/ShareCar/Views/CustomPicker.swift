//
//  CustomPicker.swift
//  CustomPicker
//
//  Created by 周逸文 on 17/2/21.
//  Copyright © 2017年 TZSY. All rights reserved.
//

import UIKit

class CustomPicker: UIView ,UIPickerViewDelegate,UIPickerViewDataSource{
    var zdefaultSelected: ((_ defaultname: String)->())?
    var bgV: UIView!
    var picker: UIPickerView!
    var toolsv: UIView!
    var cancel: UIButton!
    var sure: UIButton!
    var arrays: Array<String>!
    var OneRow = 0
    var TwoRow = 0
    
    var selectStr: String?
    
    init(frame: CGRect,items: Array<String>) {
        super.init(frame: frame)
        backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        arrays = items
        initUI()
        showPicker()
    }
    
    func initUI() {
        bgV = UIView()
        bgV.backgroundColor = UIColor.white
        toolsv = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: 40))
        toolsv.backgroundColor = UIColor.white
        cancel = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 40))
        cancel.setTitle("取消", for: .normal)
        cancel.setTitleColor(UIColor(red: 253/255, green: 128/255, blue: 35/255,alpha: 1), for: .normal)
        cancel.addTarget(self, action: #selector(CustomPicker.doCancel), for: .touchUpInside)
        sure = UIButton(frame: CGRect(x: kScreenWidth - 50, y: 0, width: 50, height: 40))
        sure.setTitle("确定", for: .normal)
        sure.setTitleColor(UIColor(red: 253/255, green: 128/255, blue: 35/255,alpha: 1), for: .normal)
        sure.addTarget(self, action: #selector(CustomPicker.doSure), for: .touchUpInside)
        self.addSubview(bgV)
        bgV.addSubview(toolsv)
        
        
        picker = UIPickerView(frame: CGRect(x: 0, y: 40, width: kScreenWidth, height: 200))
        picker.delegate = self
        picker.dataSource = self
        bgV.addSubview(picker)
        
        toolsv.addSubview(cancel)
        toolsv.addSubview(sure)
        bgV.frame = CGRect(x: 0, y: kScreenHeight, width: kScreenWidth, height: 240)
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //        super.touchesBegan(touches, with: event)
        hidePicker()
    }
    @objc func doCancel() {
        hidePicker()
    }
    @objc func doSure() {
        
        if let string = selectStr {
           zdefaultSelected?(string)
        } else {
           zdefaultSelected?(arrays[0])
        }
        hidePicker()
    }
    
    func showPicker() {
        UIView.animate(withDuration: 0.25) {
            self.bgV.frame = CGRect(x: 0, y: kScreenHeight - 240, width: kScreenWidth, height: 240)
        }
    }
    
    func hidePicker() {
        UIView.animate(withDuration:  0.25, animations: {
            self.bgV.frame = CGRect(x: 0, y: kScreenHeight, width: kScreenWidth, height: 240)
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
        
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return arrays.count

    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {

            return arrays[row]

    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectStr = self.arrays[row]
    }
}
