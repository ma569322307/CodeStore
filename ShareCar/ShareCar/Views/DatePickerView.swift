//
//  DatePickerView.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2018/11/28.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import Foundation

class DatePickerView: UIView {
    @IBOutlet weak var pickView: UIPickerView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var sureButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
