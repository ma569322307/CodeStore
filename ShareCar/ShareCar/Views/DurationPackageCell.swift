//
//  DurationPackageCell.swift
//  ShareCar
//
//  Created by lishuai on 2018/10/15.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class DurationPackageCell: UITableViewCell {

    @IBOutlet weak var borderView: UIView!

    @IBOutlet weak var packageNameLabel: UILabel!
    
    @IBOutlet weak var stadardPriceLabel: UILabel!
    
    @IBOutlet weak var monthPriceLabel: UILabel!
    
    @IBOutlet weak var yearPriceLabel: UILabel!
    
    @IBOutlet weak var bg: UIImageView!
    
    var item: Order.DurationPackageList.Item?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        if selected {
            borderView.layer.borderColor = UIColor(hexString: "ffa329")?.cgColor
            borderView.backgroundColor = UIColor(hexString: "ffffe6")
            bg.image = UIImage(named: "cell_bg")
        } else {
            borderView.layer.borderColor = UIColor(hexString: "8182a3")?.cgColor
            borderView.backgroundColor = UIColor(hexString: "ffffff")
            bg.image = UIImage(named: "cell_bg2")
        }
    }
    
    func configure(item: Order.DurationPackageList.Item) {
        self.item = item
        packageNameLabel.text = "\(item.hour)小时"
        stadardPriceLabel.text = item.standardPrice
        monthPriceLabel.text = item.monthPrice
        yearPriceLabel.text = item.yearPrice
    }

}
