//
//  OrderedCell.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/8.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class OrderedCell: UITableViewCell {
    
    @IBOutlet weak var dirveDuration: UILabel!
    @IBOutlet weak var dirveDistanceLabel: UILabel!
    @IBOutlet weak var carNumberLabel: UILabel!
    @IBOutlet weak var carNameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var orderSnLabel: UILabel!
    
    @IBOutlet weak var carImageUrl: UIImageView!
    
    func config(_ model : User.TripList.TripInfo) {
        let distance = Int(ceil(Double(model.drivingDistance) / 1000.0))
        let imageUrl = URL(string: kImageServerName + model.imagePath)
        var text = ""
        switch model.status {
        case .reserve: text = "已预定"
        case .cancel: text = model.payStatus == .unFinish ? "已取消待支付" : "已取消"
        case .using: text = "使用中"
        case .confrimReturnCar, .confrimPay: text = "待支付"
        case .finished:   text = "已支付"
        }
        timeLabel.text = model.created
        carNameLabel.text       = model.plateNumber
        dirveDistanceLabel.text = "\(distance)公里"
        dirveDuration.text      = "\(model.drivingDuration)分钟"
        carNumberLabel.text     = model.carTypeName
        carImageUrl.kf.setImage(with: imageUrl)
        orderSnLabel.text = model.ordersn
        statusLabel.text        = model.forceFlag == .forcing ? "待结单" :
                                  model.debtFlag == .debted ? "欠款中" :  text

    }
}
