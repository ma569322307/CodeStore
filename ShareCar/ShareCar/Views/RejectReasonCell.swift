//
//  RejectReasonCell.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2019/2/20.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit
import SnapKit
class RejectReasonCell: UICollectionViewCell {

    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var backgroundButton: UIButton!
    var block: (()->())!
    var model: User.RejectDetail.RejectInfo! {
        didSet {
            print(":::",kServerName + model.rejectOptionImage)
            backgroundButton.setBackgroundImageWith(URL(string: kImageServerName + model.rejectOptionImage), for: .normal, options: .ignoreDiskCache)
            switch model.rejectOption {
            case 1:tipLabel.text = "身份证正页"
                case 2:tipLabel.text = "身份证副业"
                case 3:tipLabel.text = "驾驶证正页"
                case 4:tipLabel.text = "驾驶证副业"
                case 5:tipLabel.text = "手持身份证"
            default:break
            }
        }
    }
    @IBAction func unGoodInfoImage(_ sender: Any) {
        block()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
