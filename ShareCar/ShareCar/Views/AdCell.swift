//
//  AdCell.swift
//  ShareCar
//
//  Created by 李帅 on 2019/1/18.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class AdCell: UITableViewCell {

    @IBOutlet weak var onlineTimeLabel: UILabel!
    
    @IBOutlet weak var shareTitleLabel: UILabel!
    
    @IBOutlet weak var appImagePathImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
