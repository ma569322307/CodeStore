//
//  BalanceDetailCell.swift
//  ShareCar
//
//  Created by lishuai on 2018/8/31.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit

class BalanceDetailCell: UITableViewCell {

    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var createdAtLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(item: User.BalanceDetail.Item) {
        typeLabel.text = item.useType
        moneyLabel.text = item.money
        createdAtLabel.text = item.createdAt
        
        if item.moneyType == 0 {
            moneyLabel.textColor = UIColor(hexString: "45c664")
        } else {
            moneyLabel.textColor = UIColor(hexString: "333333")
        }
    }

}
