//
//  CheckCarCell.swift
//  ShareCar
//
//  Created by lishuai on 2018/11/9.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class CheckCarCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
}
