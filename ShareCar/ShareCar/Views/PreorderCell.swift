//
//  PreorderCell.swift
//  ShareCar
//
//  Created by 李帅 on 2019/2/15.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class PreorderCell: UITableViewCell {

    @IBOutlet weak var preorderSnLabel: UILabel!
    
    @IBOutlet weak var carTypeNameLabel: UILabel!
    
    @IBOutlet weak var useCarTimeLabel: UILabel!
    
    @IBOutlet weak var useCarAddressLabel: UILabel!
    
    @IBOutlet weak var preorderStatusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(item: Preorder.List.Item) {
        preorderSnLabel.text = item.preorderSn
        carTypeNameLabel.text = item.carTypeName
        let time = Date(timeIntervalSince1970: TimeInterval(item.useCarTime))
        useCarTimeLabel.text = Date.dateConvertString(date: time)
        useCarAddressLabel.text = item.useCarAddress
        preorderStatusLabel.text = Preorder.status[item.preorderStatus]
    }
}
