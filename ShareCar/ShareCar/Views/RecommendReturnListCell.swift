//
//  RecommendReturnListCell.swift
//  ShareCar
//
//  Created by lishuai on 2018/7/2.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit

class RecommendReturnListCell: UITableViewCell {

    @IBOutlet weak var parkNameLabel: UILabel!
    @IBOutlet weak var parkingLotDistanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
