//
//  MessageCenterCellTableViewCell.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/10/29.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class MessageCenterCell: UITableViewCell {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
}
