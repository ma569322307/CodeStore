//
//  BankCell.swift
//  ShareCar
//
//  Created by 李帅 on 2019/1/14.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class BankCell: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
