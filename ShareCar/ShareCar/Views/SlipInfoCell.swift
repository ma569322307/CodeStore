//
//  SlipInfoCell.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/10.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class SlipInfoCell: UITableViewCell {
    
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var carTypeLabel: UILabel!
    @IBOutlet weak var carNumberLabel: UILabel!
    @IBOutlet weak var thingLabel: UILabel!
    @IBOutlet weak var sexLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configItem(model: User.falseDriveInfo.DriveInfo) {
        self.dateLabel.text = Date.timeStampToString(timeStamp: model.occ_time)
        self.scoreLabel.text = "计\(model.score)分"
        self.moneyLabel.text = "罚\(model.fine)元"
        self.carTypeLabel.text = model.plate_number
        self.carNumberLabel.text = model.app_car_type_name
        self.addressLabel.text = model.address
        self.thingLabel.text = model.reason
        
    }
}
