//
//  JZDatePickerView.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2018/11/28.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit
import SnapKit
class JZDatePickerView: UIView {
    
    var dataSource: Car.DayInfo!
    var whiteView: DatePickerView!
    static var block: ((Int) -> ())!
    
    static func createDatepickerViewWith(dataSource:Car.DayInfo, completionBlock: @escaping ((Int) ->())) -> JZDatePickerView {
        block = completionBlock
        return JZDatePickerView(frame: UIScreen.main.bounds, dataSource: dataSource)
    }

    init(frame: CGRect,dataSource:Car.DayInfo) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        self.dataSource = dataSource

        let chooseView = Bundle.main.loadNibNamed("DatePickerView", owner: self, options: [:])?.last
        
        if let chooseView = chooseView as? DatePickerView {
            chooseView.frame = CGRect(x: 0, y: self.height, width: kScreenWidth, height: 280)
            chooseView.cancelButton.addTarget(self, action: #selector(click), for: .touchUpInside)
            chooseView.sureButton.addTarget(self, action: #selector(click), for: .touchUpInside)
            chooseView.pickView.delegate = self
            chooseView.pickView.dataSource = self
            self.addSubview(chooseView)
            whiteView = chooseView
            //动画前的准备
            UIView.animate(withDuration: 0.25, animations: {
                self.backgroundColor = UIColor(valueRGB: 0x000000, alpha: 0.25)
                self.whiteView.top = self.height - self.whiteView.height
            })
        }
        UIApplication.shared.keyWindow?.addSubview(self)
    }
    @objc func click(sender: UIButton) {
        if sender.tag == 101 {
            let value = whiteView.pickView.selectedRow(inComponent: 0)
            JZDatePickerView.block(value)
        }
        hide()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        hide()
    }
    
    func hide() {
        UIView.animate(withDuration: 0.25, animations: {
            self.whiteView.frame = CGRect(x: 0, y: self.height, width: kScreenWidth, height: 280)
            self.backgroundColor = UIColor.clear
        }) { finsh in
            self.removeFromSuperview()
        }
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension JZDatePickerView: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.list.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        pickerView.subviews[1].backgroundColor = UIColor(white: 0.0, alpha: 0.3)
        pickerView.subviews[2].backgroundColor = UIColor(white: 0.0, alpha: 0.3)
        return "\(dataSource.list[row].day)天"
    }

}

