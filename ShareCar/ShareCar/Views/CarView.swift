//
//  CarView.swift
//  ShareCar
//
//  Created by lishuai on 2018/6/27.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit

class CarView: UIView {
    enum RouterType: Int, Codable {
        case lookCarDetailInfoType
        case confirmPreodrderCarType
    }

    @IBOutlet weak var plateNumber: UILabel!
    @IBOutlet weak var carTypeName: UILabel!
    @IBOutlet weak var carImg: UIImageView!
    @IBOutlet weak var balanceOil: UILabel!
    @IBOutlet weak var oilPercentage: UIImageView!
    @IBOutlet weak var perkmUnitPrice: UILabel!
    @IBOutlet weak var perminUnitPrice: UILabel!
    @IBOutlet weak var initiatePrice: UILabel!
    @IBOutlet weak var carAddress: UILabel!
    
    @IBOutlet weak var limitTodayImage: UIImageView!
    @IBOutlet weak var limitTomorrowImage: UIImageView!
    @IBOutlet weak var offerImage: UIImageView!
    
    @IBOutlet weak var topUserRetarnAddress: UILabel!
    var path: RouterType = .lookCarDetailInfoType
    
    var car: Car.Detail!
    
    func setCar(car: Car.Detail) {
        self.car = car
        
        let index1 = car.plateNumber.index(car.carTypeName.startIndex, offsetBy: 2)
        let index2 = car.plateNumber.index(car.carTypeName.startIndex, offsetBy: 3)
        let value =  car.plateNumber.replacingCharacters(in: index1...index2, with: "**");
        
        self.plateNumber.text = value
        self.carTypeName.text = car.carTypeName
        
        let url = URL(string: kImageServerName + car.carImg)
        self.carImg.kf.setImage(with: url)
        
        let percentage = Int(Float(car.balanceOli)! / Float(car.totalOli)! * 10)
        self.oilPercentage.image = UIImage.init(named: "oil_\(percentage)")
        self.balanceOil.text = "续航\(car.balanceOli)KM"
//        self.perkmUnitPrice.text = "\(car.priceData.perkmUnitPrice)元"
//        self.perminUnitPrice.text = "\(car.priceData.perminUnitPrice)元"
//        self.initiatePrice.text = "\(car.priceData.initiatePrice)元"
        self.carAddress.text = car.carAddress
        self.limitTodayImage.isHidden = car.limitToday == 0
        self.limitTomorrowImage.isHidden = car.limitTomorrow == 0
        
//        let offerUrl = URL(string: kServerName + ".." + car.priceData.offerImg)
//        self.offerImage.kf.setImage(with: offerUrl)
//
        if car.limitToday == 0 && car.limitTomorrow == 1 {
            self.limitTomorrowImage.snp.makeConstraints { make in
                make.left.equalTo(self.plateNumber.snp.right).offset(10)
            }
        }
        
        if path == .confirmPreodrderCarType, let label = self.viewWithTag(1000) as? UILabel {
            var address = String()
            switch car.topRetarnPos {
            case 1: address = "地面"
            case 2: address = "地下"
            case 3: address = "停车楼"
            default: address = "未知"
            }
            label.text = address + "  \(car.topRetarnLocation)"
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
