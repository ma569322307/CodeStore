
//
//  CouponDayCell.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2019/1/23.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class CouponDayCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var actionCodeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func config(item: User.QuaryList.Item) {
        nameLabel.text = item.dayCouponName
        priceLabel.text = item.couponPrice
        actionCodeLabel.text = "活动码:\(item.eventSn)"
        timeLabel.text = "有效期至\(Date.timeStampToString(timeStamp: item.useEndTime))"
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
