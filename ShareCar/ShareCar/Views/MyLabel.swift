//
//  MyLabel.swift
//  ShareCar
//
//  Created by 李帅 on 2018/12/5.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class MyLabel: UILabel {

    enum VerticalAlignment: Int {
        case Top = 0
        case Middle
        case Bottom
    }

    var verticalAlignment: VerticalAlignment = .Top
    
    func setVerticalAlignment(verticalAlignment: VerticalAlignment) {
        self.verticalAlignment = verticalAlignment
        setNeedsDisplay()
    }
    
    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        var textRect = super.textRect(forBounds: bounds, limitedToNumberOfLines: numberOfLines)
        
        switch verticalAlignment {
        case .Top:
            textRect.origin.y = bounds.origin.y
        case .Middle:
            textRect.origin.y = bounds.origin.y + bounds.size.height - textRect.size.height
        case .Bottom:
            textRect.origin.y = bounds.origin.y + (bounds.size.height - textRect.size.height) / 2.0
        }
        
        return textRect
    }
    
    override func drawText(in rect: CGRect) {
        let actualRect = textRect(forBounds: rect, limitedToNumberOfLines: numberOfLines)
        super.drawText(in: actualRect)
    }
}
