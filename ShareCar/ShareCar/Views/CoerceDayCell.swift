//
//  CoerceDayCell.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2018/12/7.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class CoerceDayCell: UITableViewCell {
    
    @IBOutlet weak var btn: UIButton!
    var block: (() ->())!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func touch(_ sender: UIButton) {
        self.block()
        
    }
    
    func configCell(block: @escaping (() -> ())) {
        self.block = block
    }
    
}
