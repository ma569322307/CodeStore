//
//  CardCell.swift
//  ShareCar
//
//  Created by jiuzhongkeji on 2019/1/7.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import UIKit

class CardCell: UITableViewCell {

    @IBOutlet weak var bankIconImageView: UIImageView!
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var cardTypeLabel: UILabel!
    @IBOutlet weak var cardNumberLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func config(item: User.CardList.CardInfo) {
        let offerUrl = URL(string: kImageServerName + "/" + item.imagePath)
        bankIconImageView.kf.setImage(with: offerUrl)
        bankNameLabel.text = item.bankName
        cardTypeLabel.text = "信用卡"
        cardNumberLabel.text = item.creditNumber
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
