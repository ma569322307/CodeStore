//
//  MineUnPayDebtCell.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/10/30.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import UIKit

class MineUnPayDebtCell: UITableViewCell {

    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var orderId: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    func config(item: User.UnpayRecard.Info) {
        self.timeLabel.text = item.created_at
        self.statusLabel.text = item.pay_status == 1 ? "未支付":"已支付"
        self.orderId.text = item.receipt_sn
        self.moneyLabel.text = item.money
        self.reasonLabel.text = item.reason
        
    }
}
