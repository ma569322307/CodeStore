//
//  CouponCell.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/9.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
class CouponCell: UITableViewCell {
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var twoTitle: UILabel!
    @IBOutlet weak var threeTitle: UILabel!
    @IBOutlet weak var oneTitle: UILabel!
    @IBOutlet weak var moneyLabel: UILabel!
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var bg: UIImageView!
    
    func configItemWithInfo(model: User.CouponList.Coupon) {
        self.title.text = model.couponName
        self.oneTitle.text = model.brief
        self.threeTitle.text = model.expireTime
        self.moneyLabel.text = model.superpositionFlag == 1 ? "不可叠加" : "可叠加"
        
        if model.useFlag == 1 {
            title.textColor = UIColor(hexString: "0a0b41")
            threeTitle.textColor = UIColor(hexString: "6c6c8d")
            moneyLabel.textColor = UIColor(hexString: "6c6c8d")
            twoTitle.textColor = UIColor(hexString: "0a0b41")
            backGroundView.borderColor = UIColor(rgb: 0x8182a3)
            bg.image = UIImage(named: "cell_bg2")
        } else {
            title.textColor = UIColor(hexString: "9d9d9d")
            threeTitle.textColor = UIColor(hexString: "9d9d9d")
            moneyLabel.textColor = UIColor(hexString: "a7a7a7")
            twoTitle.textColor = UIColor(hexString: "a7a7a7")
            backGroundView.borderColor = UIColor(rgb: 0xeaeaea)
            bg.image = UIImage(named: "cell_bg3")
        }
    }
}
