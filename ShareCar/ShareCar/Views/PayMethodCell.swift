//
//  PayMethodCell.swift
//  ShareCar
//
//  Created by lishuai on 2018/7/10.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit

class PayMethodCell: UITableViewCell {

    @IBOutlet weak var typeImage: UIImageView!
    @IBOutlet weak var typeNameLabel: UILabel!
    @IBOutlet weak var typeSelected: UIImageView!
    
    var type: Order.Pay.Payment = .aliPay {
        didSet {
            switch type {
            case .aliPay:
                typeImage.image = UIImage(named: "alipay")
                typeNameLabel.text = "支付宝支付"
            case .wePay:
                typeImage.image = UIImage(named: "wxpay")
                typeNameLabel.text = "微信支付"
            case .balance:
                typeImage.image = UIImage(named: "yue")
                typeNameLabel.text = "余额支付"
            case .ticket:
                typeImage.image = UIImage(named: "ticket")
                typeNameLabel.text = "日租预售券"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        typeSelected.image = UIImage(named: selected ? "select" : "unSelect")
    }
}

class PayMethodCell2: PayMethodCell {
    @IBOutlet weak var textField: UITextField!
}
