//
//  BlueToothManager.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/6/26.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation
import CoreBluetooth

class BlueToothManager:NSObject,CBCentralManagerDelegate,CBPeripheralDelegate{
    private var centralManager: CBCentralManager?
    private var peripheral: CBPeripheral?
    private var characteristic: CBCharacteristic?
    override init() {
        super.init()
        centralManager = CBCentralManager(delegate: self, queue: nil, options: nil)
    }
    
    // 判断手机蓝牙状态
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        
        switch central.state {
        case .unknown:
            print("未知的")
        case .resetting:
            print("重置中")
        case .unsupported:
            print("不支持")
        case .unauthorized:
            print("未验证")
        case .poweredOff:
            print("未启动")
        case .poweredOn:
            print("可用")
            self.centralManager!.scanForPeripherals(withServices: [CBUUID.init()], options: nil)
        }
    }
    /** 发现符合要求的外设 */
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        print("找到的设备:",peripheral)
        self.peripheral = peripheral
        central.connect(peripheral, options: nil)
    }
}

