//
//  AppDelegate.swift
//  ShareCar
//
//  Created by lishuai on 2018/6/21.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import UIKit
import AMapFoundationKit
import IQKeyboardManagerSwift
import AipOcrSdk
import IDLFaceSDK
import UserNotifications
import Bugly
//支付回调
typealias PaycompletionBlock = (Any) -> ()

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, WXApiDelegate {
    
    var window: UIWindow?
    //回调信息
    var completionBlock: PaycompletionBlock?
    
    var userLocation: CLLocationCoordinate2D?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let deleteToken = "DELETE_TOKEN"
        let version = UserDefaults.standard.string(forKey: deleteToken)
        if version == nil || version! != UIDevice.current.appVersion() {
            User.logout()
            UserDefaults.standard.set(UIDevice.current.appVersion(), forKey: deleteToken)
        }
        
        //高德地址的配置
        AMapServices.shared().apiKey = kAMapKey
        
        //键盘管理的相关配置
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        
        //百度文字识别
        AipOcrService.shard().auth(withAK: "oSTRLkCkyOQW43zGBsj8idCG", andSK: "KxF2BsanGzmtDfOjVatLRn8sSztSavxI")
        
        //活体识别设置
        let licensePath = Bundle.main.path(forResource: "idl-license", ofType: "face-ios")
        FaceSDKManager.sharedInstance().setLicenseID("ShareCar-face-ios", andLocalLicenceFile: licensePath)
        
        //微信应用注册
        WXApi.registerApp("wxa2a1874ee3c2d2b8")
        
        //极光
        let entity = JPUSHRegisterEntity()
        
        entity.types = Int(JPAuthorizationOptions.alert.rawValue) |
            Int(JPAuthorizationOptions.badge.rawValue) |
            Int(JPAuthorizationOptions.sound.rawValue)
        
        JPUSHService.register(forRemoteNotificationConfig: entity, delegate: self)
        
        #if DEBUG
        JPUSHService.setup(withOption: launchOptions,
                           appKey: "69ec8cd8b57c689bd1f4f900",
                           channel: "App Store",
                           apsForProduction: false)
        #else
        JPUSHService.setup(withOption: launchOptions,
                           appKey: "69ec8cd8b57c689bd1f4f900",
                           channel: "App Store",
                           apsForProduction: true)
        #endif
        
        application.applicationIconBadgeNumber = 0
        //腾讯bugly日志
        Bugly.start(withAppId: "304ad263fb")
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        JPUSHService.registerDeviceToken(deviceToken)
    }
    
    func payblock(temp: @escaping PaycompletionBlock) -> Void {
        self.completionBlock = temp
    }
    
    func onResp(_ resp: BaseResp!) {
        if let block = self.completionBlock {
            block(resp)
        }
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        JZLoading.hideLoading()
    }
    
    func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        return WXApi.handleOpen(url, delegate: self as WXApiDelegate)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if url.host == "safepay" {
            AlipaySDK.defaultService().processOrder(withPaymentResult: url) { result in
                if let block = self.completionBlock, let result = result {
                    block(result)
                }
            }
        }
        return WXApi.handleOpen(url, delegate: self as WXApiDelegate)
    }
}

//MARK: - JPUSHRegisterDelegate

extension AppDelegate: JPUSHRegisterDelegate {
    
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, willPresent notification: UNNotification!, withCompletionHandler completionHandler: ((Int) -> Void)!) {
        
    }
    
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, didReceive response: UNNotificationResponse!, withCompletionHandler completionHandler: (() -> Void)!) {
        
    }
    
    func jpushNotificationCenter(_ center: UNUserNotificationCenter!, openSettingsFor notification: UNNotification?) {
        
    }
}
