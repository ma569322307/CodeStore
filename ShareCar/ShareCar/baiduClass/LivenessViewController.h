//
//  LivenessViewController.h
//  IDLFaceSDKDemoOC
//
//  Created by 阿凡树 on 2017/5/23.
//  Copyright © 2017年 Baidu. All rights reserved.
//

#import "FaceBaseViewController.h"
@protocol FaceDelegate

- (void)sendImage:(UIImage *)image;

@end

@interface LivenessViewController : FaceBaseViewController

@property (nonatomic, weak) id<FaceDelegate> delegate;

- (void)livenesswithList:(NSArray *)livenessArray order:(BOOL)order numberOfLiveness:(NSInteger)numberOfLiveness and:(UIViewController *)pageViewController andType:(NSInteger)index;

@end
