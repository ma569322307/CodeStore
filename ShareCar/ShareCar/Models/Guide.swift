//
//  Guide.swift
//  ShareCar
//
//  Created by lishuai on 2018/10/29.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import Foundation

struct Guide: Codable {
    
    struct Item: Codable {
        let title: String
        let url: String
    }
    
    let list: [Item]
    let count: Int
}

extension Guide {
    static func getGuide(guideType: Int,
                         page: Int,
                         pageSize: Int,
                         success: @escaping (Guide?) -> (),
                         failure: @escaping (Message) -> ()) {
        
        let parameters: [String: Any] = ["guide_type": guideType,
                                         "page": page,
                                         "page_size": pageSize]
        
        Base<Guide>.request(URIString: "user/get_guide", parameters: parameters, success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
}
