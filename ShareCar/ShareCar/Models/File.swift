//
//  File.swift
//  
//
//  Created by lishuai on 2018/6/30.
//

import Foundation

struct File {
    struct Upload: Codable {
        let imagePath: String
        
        enum CodingKeys : String, CodingKey {
            case imagePath = "image_path"
        }
    }
}

extension File {
    static func upload(file: HTTPRequest.File,
                       success: @escaping (Upload?) -> (),
                       failure: @escaping (Message) -> ()) {
        
        Base<Upload>.upload(URIString: "upload_image", files: [file], success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
}
