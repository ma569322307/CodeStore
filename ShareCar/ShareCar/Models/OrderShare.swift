//
//  Share.swift
//  ShareCar
//
//  Created by 李帅 on 2019/2/20.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import Foundation

struct OrderShare {
    struct Scheme: Codable {
        
        /// 分享标题
        let shareTitle: String
        
        /// 分享内容文案
        let shareContent: String
        
        /// 完整图片地址
        let imagePath: String
        
        /// 领取优惠券链接
        let shareUrl: String
        
        enum CodingKeys : String, CodingKey {
            case shareTitle = "share_title"
            case shareContent = "share_content"
            case imagePath = "image_path"
            case shareUrl = "share_url"
        }
    }
}

extension OrderShare {
    
    /// 获取分享活动
    ///
    /// - Parameters:
    ///   - orderId: 订单id
    ///   - success: 成功回调
    ///   - failure: 失败回调
    static func scheme(orderId: String,
                       success: @escaping (Scheme?) -> (),
                       failure: @escaping (Message) -> ()) {
        
        let parameters: [String: Any] = ["order_id": orderId]
        
        Base<Scheme>.request(URIString: "user/query_share_scheme", parameters: parameters, success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
    
    /// 分享活动成功
    ///
    /// - Parameters:
    ///   - orderId: 订单id
    ///   - success: 成功回调
    ///   - failure: 失败回调
    static func success(orderId: String,
                       success: @escaping () -> (),
                       failure: @escaping (Message) -> ()) {
        
        let parameters: [String: Any] = ["order_id": orderId]
        
        Base<String>.request(URIString: "user/user_success_share_scheme", parameters: parameters, success: { data in
            success()
        }) { error, data in
            failure(error)
        }
    }
}
