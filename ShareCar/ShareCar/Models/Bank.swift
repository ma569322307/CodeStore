//
//  Bank.swift
//  ShareCar
//
//  Created by 李帅 on 2019/1/7.
//  Copyright © 2019 changqingkeji. All rights reserved.
//

import Foundation

struct Bank: Codable {
    let list: [Item]
    
    struct Item: Codable {
        let bankName: String
        let imagePath: String
        let creditNumber: String?
        let bankId: String
        let bankInitial: String
        
        enum CodingKeys : String, CodingKey {
            case bankName = "bank_name"
            case imagePath = "image_path"
            case creditNumber = "credit_number"
            case bankId = "bank_id"
            case bankInitial = "bank_initial"
        }
    }
    
    enum Flag: String {
        case all
        case own
    }
}

extension Bank {
    
    /// 获取用户绑定银行列表
    ///
    /// - Parameters:
    ///   - flag: 查找标识，all:所有银行，own:已绑定自己的银行
    ///   - kqpayFlag: 支持快钱 1：否，2是
    ///   - returnFlag: 支持退款 1：否，2是
    ///   - success: 成功回调
    ///   - failure: 失败回调
    static func list(flag: Flag,
                     kqpayFlag: Int? = nil,
                     returnFlag: Int? = nil,
                     success: @escaping ([Item]?) -> (),
                     failure: @escaping (Message) -> ()) {
        
        var parameters: [String: Any] = ["flag": flag]
        
        if let kqpayFlag = kqpayFlag {
            parameters["kqpay_flag"] = kqpayFlag
        }
        
        if let returnFlag = returnFlag {
            parameters["return_flag"] = returnFlag
        }
        
        Base<Bank>.request(URIString: "user/get_bank_list", parameters: parameters, success: { data in
            success(data?.list)
        }) { error, data in
            failure(error)
        }
    }
}
