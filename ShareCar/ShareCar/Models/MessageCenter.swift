//
//  MessageCenter.swift
//  ShareCar
//
//  Created by lishuai on 2018/10/29.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import Foundation

struct MessageCenter: Codable {
    
    struct Item: Codable {
        let messageContent: String
        let createdAt: Int
        
        enum CodingKeys : String, CodingKey {
            case messageContent = "message_content"
            case createdAt = "created_at"
        }
    }
    
    let list: [Item]
}

extension MessageCenter {
    static func getMessage(page: Int,
                           pageSize: Int,
                           success: @escaping ([MessageCenter.Item]?) -> (),
                           failure: @escaping (Message) -> ()) {
        
        let parameters: [String: Any] = ["page": page,
                                         "page_size": pageSize]
        
        Base<MessageCenter>.request(URIString: "user/get_message", parameters: parameters, success: { data in
            success(data?.list)
        }) { error, data in
            failure(error)
        }
    }
}
