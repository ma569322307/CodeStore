//
//  Ad.swift
//  ShareCar
//
//  Created by lishuai on 2018/8/23.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

struct Ad: Codable {
    
    struct Image: Codable {
        
        let imagePath: String
        let adURL: String
        let shareFlag: Int
        let shareTitle: String
        let shareContent: String
        let shareUrl: String
        let onlineTime: Int
        let adName: String
        let appImagePath: String
        
        enum CodingKeys : String, CodingKey {
            case imagePath = "image_path"
            case adURL = "ad_url"
            case shareFlag = "share_flag"
            case shareTitle = "share_title"
            case shareContent = "share_content"
            case shareUrl = "share_url"
            case onlineTime = "online_time"
            case adName = "ad_name"
            case appImagePath = "app_image_path"
        }
    }
    
    let list: [Image]
}

extension Ad {
    static func ad(success: @escaping (Ad?) -> (),
                   failure: @escaping (Message) -> ()) {
        Base<Ad>.request(URIString: "user/get_ad", success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
}
