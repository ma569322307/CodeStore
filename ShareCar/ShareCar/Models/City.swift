//
//  City.swift
//  ShareCar
//
//  Created by lishuai on 2018/9/17.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

struct City: Codable {
    let shortName: String
    
    enum CodingKeys : String, CodingKey {
        case shortName = "short_name"
    }
}

extension City {
    
    /// 根据当前位置获取省份简称
    ///
    /// - Parameters:
    ///   - location: 经纬度
    ///   - success: 成功回调
    ///   - failure: 失败回调
    static func short(location: CLLocationCoordinate2D,
                      success: @escaping (City?) -> (),
                      failure: @escaping (Message) -> ()) {
        
        let parameters: [String: Any] = ["lng": String(location.longitude),
                                         "lat": String(location.latitude)]
        
        Base<City>.request(URIString: "user/get_province_short", parameters: parameters, success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
}
