//
//  Car.swift
//  ShareCar
//
//  Created by lishuai on 2018/6/25.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

struct Car {
    struct Detail: Codable {
        let carId: String
        let plateNumber: String
        let carTypeName: String
        let carImg: String
        let carAddress: String
        let carDistance: Int
        let enduranceMileage: Int
        let noDeductibles: Float
        let balanceOli: String
        let totalOli: String
        let limitToday: Int
        let limitTomorrow: Int
        let priceData: Price
        let projectTimerentId: String
        let isUseableDurationPackage: Int
        let topRetarnPos: Int
        let topRetarnLocation: String
        let dailyPriceData: DayPrice
        let isSupportDaily: Int
        let isSupportTimerent: Int
        let isRedpackCar: Int
        let redpackCarData: RedpackCarData
        
        enum CodingKeys : String, CodingKey {
            case carId = "car_id"
            case plateNumber = "plate_number"
            case carTypeName = "car_type_name"
            case carImg = "car_img"
            case carAddress = "car_address"
            case carDistance = "car_distance"
            case enduranceMileage = "endurance_mileage"
            case noDeductibles = "no_deductibles"
            case balanceOli = "balance_oli"
            case totalOli = "total_oli"
            case priceData = "price_data"
            case limitToday = "limit_today"
            case limitTomorrow = "limit_tomorrow"
            case projectTimerentId = "project_timerent_id"
            case isUseableDurationPackage = "is_useable_duration_package"
            case topRetarnPos = "return_pos"
            case topRetarnLocation = "return_address"
            case dailyPriceData = "daily_price_data"
            case isSupportDaily = "is_support_daily"
            case isSupportTimerent = "is_support_timerent"
            case isRedpackCar = "is_redpack_car"
            case redpackCarData = "redpack_car_data"
        }
        
        struct Price: Codable {
            let perminUnitPrice: String
            let perkmUnitPrice: String
            let initiatePrice: String
            let offerStatus: Int
            let offerImg: String
            
            enum CodingKeys : String, CodingKey {
                case perminUnitPrice = "permin_unit_price"
                case perkmUnitPrice = "perkm_unit_price"
                case initiatePrice = "initiate_price"
                case offerStatus = "offer_status"
                case offerImg = "offer_img"
            }
        }
        
        struct DayPrice: Codable {
            let dailyPrice: String
            let preparePrice: String
            let noDeductibles: String
            
            enum CodingKeys: String, CodingKey {
                case dailyPrice = "daily_price"
                case preparePrice = "prepare_price"
                case noDeductibles = "no_deductibles"
            }
        }
        
        struct RedpackCarData: Codable {
            let activityName: String
            
            enum CodingKeys: String, CodingKey {
                case activityName = "activity_name"
            }
        }
    }
    
    struct NearbyCarList: Codable {
        
        let carList: [Car]
        let parkingList: [Parking]
        let shortest: Int
        
        enum CodingKeys : String, CodingKey {
            case carList = "car_list"
            case parkingList = "parking_lot_list"
            case shortest = "shortest"
        }
        
        struct Car: Codable {
            let carId: String
            let carLng: String
            let carLat: String
            let carIcon: Int
            
            enum CodingKeys : String, CodingKey {
                case carId = "car_id"
                case carLng = "car_lng"
                case carLat = "car_lat"
                case carIcon = "car_icon"
            }
        }
        
        struct Parking: Codable {
            let id: String
            let carNum: Int
            let lng: String
            let lat: String
            
            enum CodingKeys : String, CodingKey {
                case id = "parking_lot_id"
                case carNum = "car_num"
                case lng = "parking_lot_lng"
                case lat = "parking_lot_lat"
            }
        }
    }
    
    struct ParkingCarList: Codable {
        let parkingCarList: [Detail]
        
        enum CodingKeys : String, CodingKey {
            case parkingCarList = "parking_car_list"
        }
    }
    
    struct QueryCar: Codable {
        let carId: String
        let carLng: String
        let carLat: String
        let carIcon: Int
        
        enum CodingKeys : String, CodingKey {
            case carId = "car_id"
            case carLng = "car_lng"
            case carLat = "car_lat"
            case carIcon = "car_icon"
        }
    }
    
    struct DayInfo: Codable {
        
        let list:[DayCount]
        struct DayCount: Codable {
            let day: Int
            let avgPrice: String
            let pretotalPrice: String
            let unpretotalPrice: String
            
            enum CodingKeys: String, CodingKey {
                case day = "day"
                case avgPrice = "avg_price"
                case pretotalPrice = "pre_total_price"
                case unpretotalPrice = "no_pre_total_price"
                
            }
        }
    }
}

extension Car {
    //车辆详情
    static func detail(carId: String? = nil,
                       location: CLLocationCoordinate2D,
                       preorderId: String? = nil,
                       success: @escaping (Detail?) -> (),
                       failure: @escaping (Message) -> ()) {
        
        var parameters: [String: Any] = ["lng": String(location.longitude),
                                         "lat": String(location.latitude)]
        
        if let carId = carId {
            parameters["car_id"] = carId
        }
        
        if let preorderId = preorderId {
            parameters["preorder_id"] = preorderId
        }
        
        Base<Detail>.request(URIString: "user/get_car_detail", parameters: parameters, success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
    //车辆日租天数
    static func uniformPrice(carId: String,
                             success: @escaping (DayInfo?) -> (),
                             failure: @escaping (Message) -> ()){
        
        let parameters = ["car_id":carId]
        Base<DayInfo>.request(URIString: "user/query_daily_days_info", parameters: parameters, success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }

    }
    
    //附近车辆列表
    static func nearbyCarList(location: CLLocationCoordinate2D,
                              success: @escaping (NearbyCarList?) -> (),
                              failure: @escaping (Message) -> ()) {
        
        let parameters: [String: Any] = ["lng": String(location.longitude),
                                         "lat": String(location.latitude)]
        
        Base<NearbyCarList>.request(URIString: "user/get_nearby_car_list", parameters: parameters, success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
    
    //停车场车辆列表
    static func parkingCarList(parkingLotId: String,
                               location: CLLocationCoordinate2D,
                               success: @escaping ([Detail]?) -> (),
                               failure: @escaping (Message) -> ()) {
        
        let parameters: [String: Any] = ["parking_lot_id": parkingLotId,
                                         "lng": String(location.longitude),
                                         "lat": String(location.latitude)]
        
        Base<ParkingCarList>.request(URIString: "user/get_parking_car_list", parameters: parameters, success: { data in
            success(data?.parkingCarList)
        }) { error, data in
            failure(error)
        }
    }
    
    static func queryCar(plateNumber: String,
                         success: @escaping (QueryCar?) -> (),
                         failure: @escaping (Message) -> ()) {
        
        let parameters: [String: Any] = ["plate_number": plateNumber]
        
        Base<QueryCar>.request(URIString: "user/query_car", parameters: parameters, success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
}
