//
//  Pay.swift
//  ShareCar
//
//  Created by 久众科技 on 2018/7/13.
//  Copyright © 2018年 changqingkeji. All rights reserved.
//

import Foundation

struct Pay {
    enum UserDepositType:Int, Codable {
        case unDeposit = 1       //未交押金
        case deposited = 2       //已交押金
    }
    
    enum UserCardType: Int, Codable {
        case normalUserType = 1  //普通用户
        case monthUserType  = 2  //蜜月用户
        case yearUserType   = 3  //橙年用户
    }
    
    //微信支付
    static func wechatPay(payStr : String) {
        
        let orderInfo = self.getDictionaryFromJSONString(jsonString: payStr)
        let order = PayReq()
        order.partnerId =  orderInfo["partnerid"] as? String
        order.prepayId =   orderInfo["prepayid"] as? String
        order.package =    orderInfo["package"] as? String
        order.nonceStr =   orderInfo["noncestr"] as? String
        order.timeStamp =  UInt32(Int(orderInfo["timestamp"] as! String)!)
        order.sign =       orderInfo["sign"] as? String
        WXApi.send(order)
    }
    
    //支付宝支付
    static func zhifuPay(payStr : String) {
        AlipaySDK.defaultService().payOrder(payStr, fromScheme: "ShareCarProduct") { response in
            
            if let value = response{
                let code = value["resultStatus"] as? String
                if code == "4000"{
                    XHProgressHUD.showError("你还没有安装支付宝")
                } else {
                }
            }
        }
    }
    
    //Apple Pay 支付
    static func applePay(payStr: String,controller: UIViewController){}
    
    //修改用户押金状态
    static func changeUserDeposit(status: UserDepositType) {
        if let user = User.info() {
            user.isDeposit = status.rawValue
            User.saveUserInfo(user)
        }
    }
    
    //修改用户日租押金状态
    static func changeUserDayDeposit(status: UserDepositType) {
        if let user = User.info() {
            user.isDayDeposit = status.rawValue
            //判断是否需要同时修改时租押金是否同时缴纳
            if user.memberType != 3 && user.isDeposit == 1 {
                user.isDeposit = 2
            }
            User.saveUserInfo(user)
        }
    }
    
    //修改用户头像更新
    static func changeUserphoto(photoAddress: String) {
        if let user = User.info() {
            user.avatarPath = photoAddress
            User.saveUserInfo(user)
        }
    }
    
    static func getDictionaryFromJSONString(jsonString:String) ->NSDictionary{
        
        let jsonData:Data = jsonString.data(using: .utf8)!
        
        let dict = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)
        if dict != nil {
            return dict as! NSDictionary
        }
        return NSDictionary()
    }
    
    struct RechargeOption: Codable {
        let rechargeOptionList: [List]
        let balanceMoney: String
        
        enum CodingKeys : String, CodingKey {
            case rechargeOptionList = "recharge_option_list"
            case balanceMoney = "balance_money"
        }
        
        struct List: Codable {
            let optionName: String
            let amount: String
            
            enum CodingKeys : String, CodingKey {
                case optionName = "option_name"
                case amount = "amount"
            }
        }
    }
    
    static func rechargeOption(success: @escaping (RechargeOption?) -> (),
                               failure: @escaping (Message) -> ()) {
        Base<RechargeOption>.request(URIString: "user/get_recharge_option", success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
    
    /// 充值
    ///
    /// - Parameters:
    ///   - rechargeMoney: 充值金额
    ///   - payment: 支付方式
    ///   - success: 成功回调
    ///   - failure: 失败回调
    static func recharge(rechargeMoney: String,
                         payment: Order.Pay.Payment,
                         success: @escaping (User.PayInfo?) -> (),
                         failure: @escaping (Message) -> ()) {
        Base<User.PayInfo>.request(URIString: "user/recharge", parameters: ["recharge_money":rechargeMoney,"payment":payment.rawValue], success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
}
