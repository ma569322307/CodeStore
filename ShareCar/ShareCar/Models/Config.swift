//
//  Config.swift
//  ShareCar
//
//  Created by lishuai on 2018/10/15.
//  Copyright © 2018 changqingkeji. All rights reserved.
//

import Foundation

struct Config: Codable {
    
    /// 时长包页面提示信息
    let durationPackagePageInfo: String?
    
    /// 支付页面提示信息
    let payPageInfo: String?
    
    /// 押金、保证金说明
    let depositInstruction: String?
    
    /// 日租支付说明
    let dayPayInstruction: String?
    
    /// 日租上传油量说明
    let dayUploadOilInstruction: String?
    
    /// 验车说明
    let checkCarInstruction: String?
    
    /// 日租订单还车说明
    let dayReturnInstruction: String?
    
    /// 权益说明(退费说明)
    let dayRightInstruction: String?
    
    /// 押金超时说明押金超时说明
    let depositOverInstruction: String?
    
    /// 是否支持预约租车（0：否；1：是）
    let preorderOpenFlag: Int?
    
    /// 预约用车文案说明
    let preorderDesc: String
    
    /// 选择地点说明
    let preorderSelectPos: String
    
    /// 预约订单支付说明
    let preorderPay: String
    
    /// 预约订单详情说明
    let preorderDetail: String
    
    /// 预约订单取消用车说明
    let preorderCancel: String
    
    /// 有预约订单时重复下预约单
    let preorderRepeat: String
    
    /// 有预约订单时下日租
    let preorderRepeatOrder: String
    
    /// 预售券列表地址
    let dayCouponUrl: String
    
    enum CodingKeys : String, CodingKey {
        case durationPackagePageInfo = "duration_package_page_info"
        case payPageInfo = "pay_page_info"
        case depositInstruction = "deposit_instruction"
        case dayPayInstruction = "day_pay_instruction"
        case dayUploadOilInstruction = "day_upload_oil_instruction"
        case checkCarInstruction = "check_car_instruction"
        case dayReturnInstruction = "day_return_instruction"
        case dayRightInstruction = "day_right_instruction"
        case depositOverInstruction = "deposit_over_instruction"
        case preorderOpenFlag = "preorder_open_flag"
        case preorderDesc = "preorder_desc"
        case preorderSelectPos = "preorder_select_pos"
        case preorderPay = "preorder_pay"
        case preorderDetail = "preorder_detail"
        case preorderCancel = "preorder_cancel"
        case preorderRepeat = "preorder_repeat"
        case preorderRepeatOrder = "preorder_repeat_order"
        case dayCouponUrl = "day_coupon_url"
    }
}

extension Config {
    static func getConfig(configParam: String?,
                      success: @escaping (Config?) -> (),
                      failure: @escaping (Message) -> ()) {
        
        var parameters = [String: Any]()
        
        if let c = configParam {
            parameters["config_param"] = c
        }
        
        Base<Config>.request(URIString: "get_config", parameters: parameters, success: { data in
            success(data)
        }) { error, data in
            failure(error)
        }
    }
}
